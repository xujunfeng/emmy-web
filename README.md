# Emmy - web interface

This is the web interface for [Emmy](https://gitlab.com/xujunfeng/emmy).

## Setting Up

1. Make sure that your machine has [Node.js](https://nodejs.org/en/)
   and [npm](https://www.npmjs.com/) installed.

2. Run the following commands:

```
git clone https://gitlab.com/xujunfeng/emmy-web.git
cd emmy-web
npm install
```

3. Make an [environment variable](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables)
   `REACT_APP_SERVER_URL`,
   which should be the URL of the
   [Emmy-Server](https://gitlab.com/xujunfeng/emmy-server)
   to which requests from the web interface should be sent.
   The best way to define `REACT_APP_SERVER_URL`
   is to use [a `.env` file](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-development-environment-variables-in-env),
   as this would allow the environment variable to be defined for this project only.

4. Run `npm start` to start an development server on localhost.

5. Run `npm run build` to build the web interface
   into a static page and scripts which can be served by any web server.
   The output will be put in the `build` directory.

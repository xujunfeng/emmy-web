import parse from 's-expression';

const parseDeclaration = (dec) => {
  switch (dec[0]) {
    case "Type":
      return {
        type: "type",
        name: dec[1],
      };

    case "Term":
      return {
        type: "term",
        termType: dec[1],
        name: dec[2],
      };

    case "Function":
      return {
        type: "function",
        name: dec[1],
        argumentTypes: dec.slice(2, dec.length - 2),
        returnType: dec[dec.length - 1],
      };

    case "Predicate":
      return {
        type: "predicate",
        name: dec[1],
        argumentTypes: dec.slice(2, dec.length),
      };

    case "Data":
      return {
        type: "data",
        name: dec[1],
        cases: dec.slice(2).map((c) => ({
          name: c[0],
          argumentTypes: c.slice(1)
        })),
      };

    default: throw new Error("Bad declaration");
  }
};

const parseTerm = (term) => {
  if (Array.isArray(term)) {
    switch (term[0]) {
      case "+": case "-": case "*": case "/":
        return {
          type: "arithmetic",
          operator: term[0],
          arguments: term.slice(1).map(parseTerm),
        };
 
      default:
        return {
          type: "application",
          function: term[0],
          arguments: term.slice(1).map(parseTerm),
        };
    }
  } else if (/^\d+$/.test(term)) {
    return {
      type: "integer",
      value: parseInt(term),
    };
  } else {
    return {
      type: "atom",
      name: term,
    };
  }
};

const parseFormula = (fml) => {
  if (Array.isArray(fml)) {
    switch (fml[0]) {
      case "=": case ">": case "<": case ">=": case "<=":
        return {
          type: "comparison",
          operator: fml[0],
          arguments: fml.slice(1).map(parseTerm),
        };
 
      case "And": case "Or": case "Implies": case "If-and-only-if":
        return {
          type: "binary",
          connective: fml[0],
          subformulae: fml.slice(1).map(parseFormula),
        };

      case "Forall": case "Exists":
        return {
          type: "quantification",
          quantifier: fml[0],
          termType: fml[1],
          term: fml[2],
          subformulae: [parseFormula(fml[3])],
        };

      case "Not":
        return {
          type: "unary",
          connective: fml[0],
          subformulae: [parseFormula(fml[1])],
        };

      case "Ind:":
        if (fml.length === 2) {
          return {
            type: "data-induction",
            subformulae: [parseFormula(fml[1])],
          };
        } else {
          return {
            type: "function-induction",
            definition: fml[1],
            subformulae: [parseFormula(fml[2])],
          };
        }

      default:
        if (!/^[A-Z][A-Za-z0-9-]*$/.test(fml[0])) {
          throw new Error("Bad formula");
        }

        return {
          type: "predicate",
          predicate: fml[0],
          arguments: fml.slice(1).map(parseTerm),
        };
    }
  } else if (fml === "Top") {
    return {
      type: "top",
    };
  } else if (fml === "Bottom") {
    return {
      type: "bottom",
    };
  } else {
    return {
      type: "proposition",
      name: fml,
    };
  }
};

const parseJustification = (just) => {
  switch (just) {
    case "Given": return { type: "given" };
    case "?": return { type: "?" };
    default: return just;
  }
}

const parseInductionCase = (c) => {
  let premises = [];
  let next;
  for (next = 0
      ; c[next] !== "Show"
      ; next += c[next + 3] === "such" ? 6 : 3) {
    if (c[next + 3] === "such") {
      premises.push({
        term: c[next + 2],
        termType: c[next + 1],
        assumption: parseFormula(c[next + 5]),
      });
    } else {
      premises.push({
        term: c[next + 2],
        termType: c[next + 1],
      });
    }
  }
  if (c[next + 2] === "By") {
    return {
      goal: parseFormula(c[next + 1]),
      premises: premises,
      justification: parseJustification(c[next + 3]),
      subproof: [],
      trivial: true,
    };
  } else {
    return {
      goal: parseFormula(c[next + 1]),
      premises: premises,
      subproof: c.slice(next + 2).map(parseStep),
      justification: [],
      trivial: false,
    };
  }
};

const parseEqualities = (raw) => {
  let equalities = [];
  let next;
  for (next = 0; next + 2 < raw.length; next = next + 3) {
    equalities = [
      ...equalities,
      { term: parseTerm(raw[next + 1]),
        justification: parseJustification(raw[next + 2]),
      }
    ];
  }
  return equalities;
};

const parseStep = (step) => {
  switch (step[1]) {
    case "=":
      return {
        type: "equalities",
        number: step[0],
        term: parseTerm(step[2]),
        equalities: parseEqualities(step.slice(3)),

        justification: [],
      };

    case "Induction":
      if (step[2] === "on") {
        return {
          type: "function-induction",
          number: step[0],
          definition: step[3],
          formula: parseFormula(step[4]),
          cases: step.slice(5).map(parseInductionCase),

          justification: [],
        };
      } else {
        return {
          type: "data-induction",
          number: step[0],
          formula: parseFormula(step[2]),
          cases: step.slice(3).map(parseInductionCase),

          justification: [],
        };
      }

    case "Take":
      if (step[4] === "such" && step[5] === "that") {
        return {
          type: "introduction-with-assumption",
          number: step[0],
          termType: step[2],
          term: step[3],
          assumption: parseFormula(step[6]),
          subproof: step.slice(7).map(parseStep),
          declarationType: "term",

          justification: [],
        };
      } else {
        return {
          type: "introduction",
          number: step[0],
          termType: step[2],
          term: step[3],
          subproof: step.slice(4).map(parseStep),
          declarationType: "term",

          justification: [],
        };
      }

    case "Assume":
      return {
        type: "assumption",
        number: step[0],
        assumption: parseFormula(step[2]),
        subproof: step.slice(3).map(parseStep),

        justification: [],
      };

    default:
      return {
        type: "step",
        number: step[0],
        formula: parseFormula(step[1]),
        justification: parseJustification(step[2]),
      };
  }
};

const parseLemma = (lem) => {
  return {
    type: "lemma",
    number: lem[0],
    formula: parseFormula(lem[1]),
  };
}

const parseRhs = (rhs) => {
  if (rhs[0] === "cases") {
    return rhs.slice(1).map((c) => ({
      condition: parseFormula(c[0]),
      body: parseTerm(c[1])
    }));
  } else if (rhs.length === 1) {
    return parseTerm(rhs[0]);
  }
}

const parseDefinition = (def) => {
  return {
    type: "definition",
    number: def[0],
    lhs: parseTerm(def[1]),
    rhs: parseRhs(def.slice(2)),
  };
}

const parseSection = (section) => {
  if (section instanceof String) {
    return {
      type: "comment",
      text: section.toString(),
    };
  }
  switch (section[0]) {
    case "Proof":
      return {
        type: "proof",
        goal: parseFormula(section[1]),
        steps: section.slice(2).map(parseStep),
      };

    case "Declare":
      return {
        type: "declare",
        declarations: section.slice(1).map(parseDeclaration),
      };

    case "Lemma":
      return {
        type: "lemmas",
        lemmas: section.slice(1).map(parseLemma),
      };

    case "Define":
      return {
        type: "define",
        definitions: section.slice(1).map(parseDefinition),
      };

    default: throw new Error("Bad section");
  }
};

const parseProgram = (raw) => {
  const processedRaw =
    raw
    .replace(/;.*$/gm, "")
    .replace(/#lang.*$/gm, "")
    .replace(/\[/g, "(")
    .replace(/\]/g, ")")
    .replace(/{/g, "(")
    .replace(/}/g, ")");
  const program = parse("(" + processedRaw + ")");
  if (!Array.isArray(program)) {
    throw new Error("Bad program format");
  }

  return program.map(parseSection);
};

export default parseProgram;

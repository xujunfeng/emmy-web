// Generates induction principle for a step

import { formatFormula } from './../util/format.js';

// Substitute a variable for another term
const substitute = (from, to) => (term) => {
  switch (term.type) {
    case "atom": return term.name === from ? to : term;

    case "application":
      return {
        ...term,
        arguments: term.arguments.map(substitute(from, to)),
      };

    case "arithmetic":
      return {
        ...term,
        arguments: term.arguments.map(substitute(from, to)),
      };

    case "quantification":
      if (term.term === from) {
        return term;
      } else if (to.type === "atom" && to.name === term.term) {
        const newTerm = {
          type: "atom",
          name: term.term + "-",
        };
        return {
          ...term,
          term: newTerm.name,
          subformulae: term.subformulae.map(substitute(term.term, newTerm))
                                       .map(substitute(from, to)),
        };
      } else {
        return {
          ...term,
          subformulae: term.subformulae.map(substitute(from, to)),
        };
      }

    case "induction":
    case "unary":
    case "binary":
      return {
        ...term,
        subformulae: term.subformulae.map(substitute(from, to)),
      };

    case "comparison":
    case "predicate":
      return {
        ...term,
        arguments: term.arguments.map(substitute(from, to)),
      };

    default: return term;
  }
};

const generateInductionPrinciple = (declarations, step) => {
  // ensure that the step is a simple step of the format
  // [N forall x: T. P(x) (...)]
  // ..where T is a data structure
  
  if (step.type !== "step") {
    throw new Error("Not a simple step.");
  }

  const formula = step.formula;

  if (formula.type !== "quantification"
      || formula.quantifier !== "Forall") {
    throw new Error("Not a universally quantified goal.");
  }

  const type = formula.termType;
  const declaration = declarations.find((dec) =>
    dec.type === "data" && dec.name === type
  );

  if (declaration === undefined) {
    throw new Error("Not a data structure");
  }

  // For each case generate a step

  const cases = declaration.cases.map((c, i) => {
    let next = {};
    const premises = c.argumentTypes.map((t) => {
      const termIndex = next[t] === undefined ? 0 : next[t] + 1;
      next[t] = termIndex;
      const termName = t[0].toLowerCase()
        + (termIndex === 0 ? "" : termIndex);
      return {
        termType: t,
        term: termName,
        assumption: t === declaration.name ?
            substitute(
              formula.term,
              { type: "atom", name: termName }
            )(formula.subformulae[0])
          : undefined,
      };
    });
    const constructed = c.argumentTypes.length === 0 ?
    {
      type: "atom",
      name: c.name,
    } : {
      type: "application",
      function: c.name,
      arguments: premises.map((p) => ({ type: "atom", name: p.term })),
    };
    const goal = substitute(formula.term, constructed)(formula.subformulae[0]);
    return {
      key: i,
      premises: premises,
      goal,
      raw: formatFormula(goal),
      subproof: [],
    };
  });

  // returns a list of steps
  return cases;
  
};

export default generateInductionPrinciple;

import React, { Component } from 'react';
import MathJax from 'react-mathjax';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { saveAs } from 'file-saver';

import './App.css';

import Comment from './Comment.js';
import Declaration from './Declaration.js';
import parseProgram from './ProgramParser.js';
import Formula from './Formula.js';
import Step from './Step.js';
import Lemma from './Lemma.js';
import Definition from './Definition.js';

import exportSection from './util/export.js'

import {
  formatFormula,
  formatDeclaration,
  formatDefinition,
  formatTerm,
} from './util/format.js';
import generateInductionPrinciple from './proof/induction.js';

import {
  parseFormula,
  parseDeclaration,
  parseDefinition,
  parseTerm,
} from './Parser.js';

const DEFAULT_STEP = {
  // No key
  type: "step",
  // No number
  formula: "Empty",
  raw: "",
  justification: { type: "?" },
};

const DEFAULT_EQUALITIES = {
  // No key
  type: "equalities",
  // No number
  term: "Empty",
  raw: "",

  equalities: [ { term: "Empty", raw: "", justification: [] }],

  justification: [],
};

const DEFAULT_DATA_INDUCTION = {
  // No key
  type: "data-induction",
  // No number
  formula: "Empty",
  raw: "",

  cases: [],

  justification: [],
};

const DEFAULT_INTRODUCTION_WITH_ASSUMPTION = {
  // No key
  type: "introduction-with-assumption",
  // No number
  assumption: "Empty",
  raw: "",
  term: "x",
  termType: "Atom",
  declarationRaw: "x : Atom",
  declarationType: "term",
  subproof: [],
  justification: [],
};

const DEFAULT_INTRODUCTION = {
  // No key
  type: "introduction",
  // No number
  term: "x",
  termType: "Atom",
  declarationRaw: "x : Atom",
  declarationType: "term",
  subproof: [],
  justification: [],
};

const DEFAULT_ASSUMPTION = {
  // No key
  type: "assumption",
  // No number
  assumption: "Empty",
  raw: "",
  subproof: [],
  justification: [],
};

var keyGenerations = {}

const nextKey = (number) => {
  if (keyGenerations[number] === undefined) {
    keyGenerations[number] = 0;
    return "GEN0:" + number.toString();
  } else {
    keyGenerations[number]++;
    return "GEN" + keyGenerations[number].toString() + ":" + number.toString();
  }
}

// Checks if there are invalid formulae or duplicate step numbers etc...
const validateProgram = (program) => {
  // eslint-disable-next-line
  const declarations = program.declarations;

  for (let section of program) {
    if (section.type === "proof") {
      const stepNumbers = section.steps.map((step) => step.number);
      let seen = [];
      let duplicates = [];
      stepNumbers.forEach((num) => {
        if (seen.includes(num)) {
          if (!duplicates.includes(num)) {
            duplicates.push(num);
          }
        } else {
          seen.push(num);
        }
      });
      if (duplicates.length > 0) {
        return {
          type: "Error",
          title: "Duplicated step numbers",
          message: `The step numbers ${duplicates.join(', ')} appeared more than once.`
        }
      }

      const invalidSteps = section.steps.filter((step) =>
        step.formula === "Empty" || step.assumption === "Empty" ||
        step.formula === "Invalid" || step.assumption === "Invalid" ||
        (step.type === "assumption" && step.assumption === undefined) ||
        (step.type === "step" && step.formula === undefined)
      );
      if (invalidSteps.length > 0) {
        return {
          type: "Error",
          title: "Invalid steps",
          message: `The steps ${invalidSteps.map((step) => step.number).join(', ')} are invalid.`
        }
      }
    }
  }

  return "Ok";
};

const mapSteps = (steps, func) => steps.map((step) => {
  switch (step.type) {
    case "function-induction":
    case "data-induction": return func({
      ...step,
      cases: step.cases.map((c) => ({
        ...c,
        subproof: c.subproof === undefined ?
          undefined : mapSteps(c.subproof, func),
      })),
    });

    case "introduction-with-assumption":
    case "introduction":
    case "assumption": return {
      ...func(step),
      subproof: mapSteps(step.subproof, func),
    };
    
    default: return func(step);
  }
});

// Add `newStep' after the step whose number is `number'
// Returns the new list of `steps'
const addAfterStep = (steps, number, newStep, within) => {
  let newSteps = [];
  for (let o = 0; o < steps.length; ++o) {
    let step = steps[o];
    if (step.number === number
        && step.type !== "step"
        && step.type !== "equalities"
        && within) {
      newSteps.push({
        ...step,
        subproof: [newStep, ...step.subproof],
      });
    } else if (step.number === number) {
      newSteps.push(step);
      newSteps.push(newStep);
    } else if (Array.isArray(step.cases)) {
      newSteps.push({
        ...step,
        cases: step.cases.map((c) => ({
          ...c,
          subproof: addAfterStep(c.subproof, number, newStep, within),
        })),
      });
    } else if (step.type !== "step" && step.type !== "equalities") {
      newSteps.push({
        ...step,
        subproof: addAfterStep(step.subproof, number, newStep, within),
      });
    } else {
      newSteps.push(step);
    }
  }
  return newSteps;
};

const deleteStep = (steps, number) => {
  let newSteps = [];
  for (let o = 0; o < steps.length; ++o) {
    let step = steps[o];
    if (step.number === number) {
    } else if (step.type === "data-induction"
               || step.type === "function-induction") {
      newSteps.push({
        ...step,
        cases: step.cases.map((c) => ({
          ...c,
          subproof: deleteStep(c.subproof, number),
        })),
      });
    } else if (step.type !== "step" && step.type !== "equalities") {
      newSteps.push({
        ...step,
        subproof: deleteStep(step.subproof, number),
      });
    } else {
      newSteps.push(step);
    }
  }
  return newSteps;
};

const replaceStepNumber = (steps, number, newNumber) => {
  const updateJustification = (num, newNum) => (stepNum) => {
    if (stepNum === num) {
      return newNum;
    } else {
      return stepNum;
    }
  };

  const updatedSteps = mapSteps(steps, (step) => {
    // We need to consistantly update its justification
    let newJustification;
    if (Array.isArray(step.justification)) {
      newJustification = step.justification.map(
        updateJustification(number, newNumber)
      );
    } else {
      newJustification = step.justification;
    }

    if (step.number === number) {
      return {
        ...step,
        key: nextKey(newNumber),
        number: newNumber,
        justification: newJustification,
      };
    } else {
      return {
        ...step,
        key: nextKey(step.number),
        justification: newJustification,
      };
    }
  });

  return updatedSteps;
};

const initialiseStep = (step) => {
  let raw = step.raw;
  let cases;
  let equalities;
  let declarationRaw = step.declarationRaw;
  let declarationType = step.declarationType;

  const initialisePremise = (p, i) => ({
    ...p,
    declarationRaw: p.term + ": " + p.termType,
    raw: p.assumption === undefined ?
      undefined : formatFormula(p.assumption),
  });
  const initialiseCase = (c, i) => ({
    ...c,
    premises: c.premises.map(initialisePremise),
    key: i,
    raw: formatFormula(c.goal),
  });
  const initialiseEquality = (e, i) => ({
    ...e,
    key: i,
    raw: formatTerm(e.term),
  });

  if (step.type === "data-induction" || step.type === "function-induction") {
    cases = step.cases.map(initialiseCase);
  } else if (step.type === "equalities") {
    equalities = step.equalities.map(initialiseEquality);
  }

  if (raw === undefined) {
    switch (step.type) {
      case "equalities":
        raw = formatTerm(step.term);
        break;

      case "function-induction":
      case "data-induction":
        raw = formatFormula(step.formula);
        break;

      case "introduction-with-assumption":
      case "assumption":
        raw = formatFormula(step.assumption);
        break;

      case "step":
        raw = formatFormula(step.formula);
        break;

      default:
        break;
    }
  } 
  if (declarationRaw === undefined) {
    switch (step.type) {
      case "introduction-with-assumption":
      case "introduction":
        declarationRaw = step.term + ": " + step.termType;
        break;

      default:
        break;
    }
  } 
  if (declarationType === undefined) {
    switch (step.type) {
      case "introduction-with-assumption":
      case "introduction":
        declarationType = "term";
        break;

      default:
        break;
    }
  }
  return {
    ...step,
    cases: cases,
    equalities: equalities,
    raw: raw,
    declarationRaw: declarationRaw,
    declarationType: declarationType,
    key: nextKey(step.number),
  };
};

const initialiseDeclaration = (dec) => ({
  ...dec,
  raw: formatDeclaration(dec),
  key: nextKey(":Dec"),
});

const initialiseLemma = (lem) => ({
  ...lem,
  raw: formatFormula(lem.formula),
  key: nextKey(":Lem"),
});

const initialiseDefinition = (def) => ({
  ...def,
  raw: formatDefinition(def),
  key: nextKey(":Def"),
});

const initialiseSection = (section) => {
  switch (section.type) {
    case "declare": return {
      ...section,
      declarations: section.declarations.map(initialiseDeclaration),
    };

    case "lemmas": return {
      ...section,
      lemmas: section.lemmas.map(initialiseLemma),
    };

    case "define": return {
      ...section,
      definitions: section.definitions.map(initialiseDefinition),
    };

    case "proof": return {
      ...section,
      steps: mapSteps(section.steps, initialiseStep),
      goalRaw: formatFormula(section.goal),
    };

    default: return {
      ...section,
    };
  }
};

// Handlers

const handleGenericDelete = (ident) => (section) => () => {
  return {
    ...section,
    declarations: section.declarations !== undefined ?
      section.declarations.filter((d) => d.key !== ident) : undefined,
    lemmas: section.lemmas !== undefined ?
      section.lemmas.filter((d) => d.key !== ident) : undefined,
    definitions: section.definitions !== undefined ?
      section.definitions.filter((d) => d.key !== ident) : undefined,
  };
};

const addDeclaration = (section) => () => {
  const newDeclaration = {
    type: "empty",
    raw: "",
    key: nextKey(":DEC"),
  };

  return {
    ...section,
    declarations: [...section.declarations, newDeclaration],
  };
};

const handleCommentUpdate = (section) => (update) => {
  return {
    ...section,
    text: update.text,
  };
};

const handleDeclarationUpdate = (section) => (update) => {
  const parseResult = parseDeclaration(update.raw);
  let type;

  if (parseResult.status) {
    type = parseResult.value.type;
  } else if (update.raw === "") {
    type = "empty";
  } else {
    type = "invalid";
  }

  const updatedDecs = section.declarations.map((dec) => {
    if (dec.key === update.ident) {
      if (parseResult.status) {
        return {
          ...parseResult.value,
          raw: update.raw,
          key: nextKey(":DEC"),
        }
      } else {
        return {
          ...dec,
          key: nextKey(":DEC"),
          type: type,
          raw: update.raw,
        };
      }
    } else {
      return dec;
    }
  });

  return {
    ...section,
    declarations: updatedDecs,
  };
};

const addLemma = (section) => () => {
  const newLemma = {
    type: "lemma",
    raw: "",
    formula: "Empty",
    key: nextKey(":Lem"),
    number: "New-Lemma",
  };

  return {
    ...section,
    lemmas: [...section.lemmas, newLemma],
  };
};

const handleLemmaUpdate = (section) => (update) => {
  if (update.raw !== undefined) {
    return handleLemmaFormulaUpdate(section)(update);
  } else if (update.newNumber !== undefined) {
    return handleLemmaNumberUpdate(section)(update);
  }

  return section;
};

const handleLemmaFormulaUpdate = (section) => (update) => {
  const parseResult = parseFormula(update.raw);
  let formula;

  if (parseResult.status) {
    formula = parseResult.value;
  } else if (update.raw === "") {
    formula = "Empty";
  } else {
    formula = "Invalid";
  }

  const updatedLemmas = section.lemmas.map((lem) => {
    if (lem.key === update.ident) {
      return {
        ...lem,
        key: nextKey(":Lem"),
        formula: formula,
        raw: update.raw,
      };
    } else {
      return lem;
    }
  });

  return {
    ...section,
    lemmas: updatedLemmas,
  };
};

// We do not rename in all steps... for now
const handleLemmaNumberUpdate = (section) => (update) => {
  const updatedLemmas = section.lemmas.map((lem) => {
    if (lem.key === update.ident) {
      return {
        ...lem,
        key: nextKey(":Lem"),
        number: update.newNumber,
      };
    } else {
      return lem;
    }
  });

  return {
    ...section,
    lemmas: updatedLemmas,
  };
}

// Definition handlers

const addDefinition = (section) => () => {
  const newDefinition = {
    type: "definition",
    raw: "",
    lhs: {},
    rhs: {},
    key: nextKey(":Def"),
    number: "New-Definition",
  };

  return {
    ...section,
    definitions: [...section.definitions, newDefinition],
  };
};

const handleDefinitionUpdate = (section) => (update) => {
  if (update.raw !== undefined) {
    return handleDefinitionBodyUpdate(section)(update);
  } else if (update.newNumber !== undefined) {
    return handleDefinitionNumberUpdate(section)(update);
  }

  return section;
};

const handleDefinitionBodyUpdate = (section) => (update) => {
  const parseResult = parseDefinition(update.raw);
  let lhs = {};
  let rhs = {};

  if (parseResult.status) {
    lhs = parseResult.value.lhs;
    rhs = parseResult.value.rhs;
  }

  const updatedDefinitions = section.definitions.map((def) => {
    if (def.key === update.ident) {
      return {
        ...def,
        key: nextKey(":Def"),
        lhs: lhs,
        rhs: rhs,
        raw: update.raw,
      };
    } else {
      return def;
    }
  });

  return {
    ...section,
    definitions: updatedDefinitions,
  };
};

// We do not rename in all steps... for now
const handleDefinitionNumberUpdate = (section) => (update) => {
  const updatedDefinitions = section.definitions.map((def) => {
    if (def.key === update.ident) {
      return {
        ...def,
        key: nextKey(":Def"),
        number: update.newNumber,
      };
    } else {
      return def;
    }
  });

  return {
    ...section,
    definitions: updatedDefinitions,
  };
}

// Step handlers

const renumberSteps = (section) => () => {
  let numbers = [];
  mapSteps(section.steps, (step) => {
    numbers.push(step.number);
  });

  let renumberedSteps = section.steps;
  numbers.forEach((num) => {
    renumberedSteps = replaceStepNumber(renumberedSteps, num, "TOCHANGE" + num);
  });

  let nextNumber = 0;
  numbers.forEach((num) => {
    ++nextNumber;
    renumberedSteps = replaceStepNumber(
      renumberedSteps,
      "TOCHANGE" + num, nextNumber.toString()
    );
  });

  return {
    ...section,
    steps: renumberedSteps,
  };
}

const handleGoalUpdate = (section) => (raw) => {
  const parseResult = parseFormula(raw);

  let formula;
  if (parseResult.status) {
    formula = parseResult.value;
  } else if (raw === "") {
    formula = "Empty";
  } else {
    formula = "Invalid";
  }

  return {
    ...section,
    goal: formula,
    goalRaw: raw,
  };
};

const handleStepDelete = (section) => (number) => {
  let updatedSteps = deleteStep(section.steps, number);

  if (updatedSteps.length === 0) {
    updatedSteps = [
      {
        key: nextKey("1"),
        type: "step",
        number: "1",
        raw: "",
        formula: "Empty",
        justification: { type: "?" },
      },
    ]
  }

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleStepAddAfter = (section) => (number, within, type) => {
  // Check if the current number is all digits
  const matches = number.match(/^[0-9]+$/);
  let newNumber
  const alreadyExists = (num) => {
    let found = false;
    mapSteps(section.steps, (step) => found |= step.number === num);
    return found;
  };

  if (matches === null) {
    newNumber = number + "-1";
  } else {
    newNumber = (parseInt(number, 10) + 1).toString();
    if (alreadyExists(newNumber)) {
      newNumber = number + "-1";
    }
  }

  while (alreadyExists(newNumber)) {
    newNumber += "-1";
  }

  let newStep;
  switch (type) {
    case "equalities":
      newStep = {
        ...DEFAULT_EQUALITIES,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "data-induction":
      newStep = {
        ...DEFAULT_DATA_INDUCTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "introduction-with-assumption":
      newStep = {
        ...DEFAULT_INTRODUCTION_WITH_ASSUMPTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "introduction":
      newStep = {
        ...DEFAULT_INTRODUCTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "assumption":
      newStep = {
        ...DEFAULT_ASSUMPTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "step":
    default:
      newStep = {
        ...DEFAULT_STEP,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;
  }
  const updatedSteps = addAfterStep(section.steps, number, newStep, within);

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleStepUpdate = (declarations) => (section, addMessage) => (update) => {
  if (update.type === "case-justification") {
    return handleCaseJustificationUpdate(section, addMessage)(update);
  }

  if (update.type === "flip") {
    return handleInductionTypeUpdate(section, addMessage)(update);
  }

  if (update.type === "induction-definition") {
    return handleInductionDefinitionUpdate(section, addMessage)(update);
  }

  if (update.type === "induction") {
    return handleInduction(declarations)(section, addMessage)(update);
  }

  if (update.type === "case-add-step") {
    return handleCaseAddStep(section, addMessage)(update);
  }

  if (update.type === "add-case") {
    return handleAddInductionCase(section, addMessage)(update);
  }

  if (update.type === "remove-case") {
    return handleRemoveInductionCase(section, addMessage)(update);
  }

  if (update.type === "add-premise") {
    return handleAddInductionCasePremise(section, addMessage)(update);
  }

  if (update.type === "remove-premise") {
    return handleRemoveInductionCasePremise(section, addMessage)(update);
  }

  if (update.type === "equality-term") {
    return handleEqualityTermUpdate(section, addMessage)(update);
  }

  if (update.type === "equality-justification") {
    return handleEqualityJustificationUpdate(section, addMessage)(update);
  }

  if (update.type === "add-equality") {
    return handleAddEquality(section, addMessage)(update);
  }

  if (update.type === "remove-equality") {
    return handleRemoveEquality(section, addMessage)(update);
  }

  if (update.type === "case-goal") {
    return handleCaseGoalUpdate(section, addMessage)(update);
  }

  if (update.type === "case-flip") {
    return handleCaseFlip(section, addMessage)(update);
  }

  if (update.type === "premise-formula") {
    return handlePremiseFormulaUpdate(section, addMessage)(update);
  }

  if (update.type === "premise-introduction") {
    return handlePremiseIntroductionUpdate(section, addMessage)(update);
  }

  if (update.raw !== undefined) {
    if (update.type === "introduction") {
      return handleIntroductionUpdate(section, addMessage)(update);
    } else {
      return handleFormulaUpdate(section, addMessage)(update);
    }
  }

  if (update.newNumber !== undefined) {
    return handleNumberUpdate(section, addMessage)(update);
  }

  if (update.justificationRaw !== undefined) {
    return handleJustificationUpdate(section, addMessage)(update);
  }

  return section;
};

const handleInductionTypeUpdate = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        type: update.to,
        status: undefined,
        definition: "Function definition number",
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleInductionDefinitionUpdate = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        type: update.newDefinition.length === 0 ?
          "data-induction" : "function-induction",
        status: undefined,
        definition: update.newDefinition,
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleCaseAddStep = (section) => (update) => {
  const number = update.number + "-case-" + (update.caseIndex + 1);
  // Check if the current number is all digits
  const matches = number.match(/^[0-9]+$/);
  let newNumber
  const alreadyExists = (num) => {
    let found = false;
    mapSteps(section.steps, (step) => found |= step.number === num);
    return found;
  };

  if (matches === null) {
    newNumber = number + "-1";
  } else {
    newNumber = (parseInt(number, 10) + 1).toString();
    if (alreadyExists(newNumber)) {
      newNumber = number + "-1";
    }
  }

  while (alreadyExists(newNumber)) {
    newNumber += "-1";
  }

  let newStep;
  switch (update.stepType) {
    case "introduction-with-assumption":
      newStep = {
        ...DEFAULT_INTRODUCTION_WITH_ASSUMPTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "introduction":
      newStep = {
        ...DEFAULT_INTRODUCTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "assumption":
      newStep = {
        ...DEFAULT_ASSUMPTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "equalities":
      newStep = {
        ...DEFAULT_EQUALITIES,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "data-induction":
      newStep = {
        ...DEFAULT_DATA_INDUCTION,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;

    case "step":
    default:
      newStep = {
        ...DEFAULT_STEP,
        key: nextKey(newNumber),
        number: newNumber,
      };
      break;
  }

  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    subproof: [
      newStep,
      ...c.subproof,
    ],
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleInduction = (declarations) => (section, addMessage) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number && step.type === "step") {
      return {
        ...step,
        status: undefined,
        type: "data-induction",
        key: nextKey(update.number),
        cases: generateInductionPrinciple(declarations, step),
        justification: [],
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleAddInductionCase = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: [
          ...step.cases,
          { trivial: false,
            premises: [],
            goal: "Empty",
            raw: "",
            subproof: [],
            justification: [],
            key: nextKey(step.cases.length) }
        ],
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleRemoveInductionCase = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: [
          ...step.cases.slice(0, update.caseIndex),
          ...step.cases.slice(update.caseIndex + 1),
        ]
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleCaseJustificationUpdate = (section) => (update) => {
  let justification = update.raw.trim();
  if (justification.toLowerCase() === "given") {
    justification = { type: "given" };
  } else if (justification === "?") {
    justification = { type: "?" };
  } else {
    justification = justification.split(" ").filter((j) => j.length > 0);
  }

  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    justification: justification
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleAddInductionCasePremise = (section) => (update) => {
  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    premises: [
      ...c.premises,
      { assumption: update.assumption ? "Empty" : undefined,
        raw: update.assumption ? "" : undefined,
        term: "x",
        termType: "Atom",
        declarationRaw: "x : Atom",
      }
    ],
  } : c);
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleRemoveInductionCasePremise = (section) => (update) => {
  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    premises: [
      ...c.premises.slice(0, update.premiseIndex),
      ...c.premises.slice(update.premiseIndex + 1),
    ],
  } : c);
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleRemoveEquality = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number && step.type === "equalities") {
      const equalities = [
        ...step.equalities.slice(0, update.equalityIndex),
        ...step.equalities.slice(update.equalityIndex + 1),
      ];
      return {
        ...step,
        status: undefined,
        equalities: (equalities.length > 0 ? equalities : [
          { term: "Empty", raw: "", justification: [] },
        ]),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleAddEquality = (section) => (update) => {
  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number && step.type === "equalities") {
      return {
        ...step,
        status: undefined,
        equalities: [
          ...step.equalities,
          { term: "Empty", raw: "", justification: [] }
        ],
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleEqualityTermUpdate = (section) => (update) => {
  const parseResult = parseTerm(update.raw);
  let term;

  if (parseResult.status) {
    term = parseResult.value;
  } else if (update.raw === "") {
    term = "Empty";
  } else {
    term = "Invalid";
  }

  const handleEquality = (e, i) => (i === update.equalityIndex ? {
    ...e,
    term: term,
    raw: update.raw,
  } : e);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number && step.type === "equalities") {
      return {
        ...step,
        status: undefined,
        equalities: step.equalities.map(handleEquality),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleEqualityJustificationUpdate = (section) => (update) => {
  let justification = update.raw.trim();
  if (justification.toLowerCase() === "given") {
    justification = { type: "given" };
  } else if (justification === "?") {
    justification = { type: "?" };
  } else {
    justification = justification.split(" ").filter((j) => j.length > 0);
  }

  const handleEquality = (e, i) => (i === update.equalityIndex ? {
    ...e,
    justification: justification,
  } : e);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number && step.type === "equalities") {
      return {
        ...step,
        status: undefined,
        equalities: step.equalities.map(handleEquality),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleCaseGoalUpdate = (section) => (update) => {
  const parseResult = parseFormula(update.raw);
  let formula;

  if (parseResult.status) {
    formula = parseResult.value;
  } else if (update.raw === "") {
    formula = "Empty";
  } else {
    formula = "Invalid";
  }

  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    goal: formula,
    raw: update.raw,
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleCaseFlip = (section) => (update) => {
  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    trivial: !c.trivial,
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handlePremiseFormulaUpdate = (section) => (update) => {
  const parseResult = parseFormula(update.raw);
  let formula;

  if (parseResult.status) {
    formula = parseResult.value;
  } else if (update.raw === "") {
    formula = "Empty";
  } else {
    formula = "Invalid";
  }

  const handlePremise = (p, i) => (i === update.premiseIndex ? {
    ...p,
    assumption: formula,
    raw: update.raw,
  } : p);

  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    premises: c.premises.map(handlePremise),
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handlePremiseIntroductionUpdate = (section) => (update) => {
  const parseResult = parseDeclaration(update.raw);
  let declaration;

  if (parseResult.status && parseResult.value.type === "term") {
    declaration = parseResult.value;
  } else if (update.raw === "") {
    declaration = {
      type: "empty",
    };
  } else {
    declaration = {
      type: "invalid",
    };
  }

  declaration.raw = update.raw;

  const handlePremise = (p, i) => (i === update.premiseIndex ? {
    ...p,
    term: declaration.name,
    termType: declaration.termType,
    declarationRaw: update.raw,
    declarationType: declaration.type,
  } : p);

  const handleCase = (c, i) => (i === update.caseIndex ? {
    ...c,
    premises: c.premises.map(handlePremise),
  } : c);

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number
        && (step.type === "data-induction"
            || step.type === "function-induction")) {
      return {
        ...step,
        status: undefined,
        cases: step.cases.map(handleCase),
        key: nextKey(update.number),
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleFormulaUpdate = (section) => (update) => {
  const parseResult = parseFormula(update.raw);
  let formula;

  if (parseResult.status) {
    formula = parseResult.value;
  } else if (update.raw === "") {
    formula = "Empty";
  } else {
    formula = "Invalid";
  }

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number) {
      switch (step.type) {
        case "equalities":
          const termParseResult = parseTerm(update.raw);
          let term;

          if (termParseResult.status) {
            term = termParseResult.value;
          } else if (update.raw === "") {
            term = "Empty";
          } else {
            term = "Invalid";
          }
          return {
            ...step,
            status: undefined,
            key: nextKey(update.number),
            term: term,
            raw: update.raw,
          };

        case "step":
          return {
            ...step,
            status: undefined,
            key: nextKey(update.number),
            formula: formula,
            raw: update.raw,
          };

        case "data-induction":
        case "function-induction":
          return {
            ...step,
            status: undefined,
            key: nextKey(update.number),
            formula: formula,
            raw: update.raw,
          };

        case "introduction-with-assumption":
        case "assumption":
        return {
          ...step,
          key: nextKey(update.number),
          assumption: formula,
          raw: update.raw,
        };

        default: return step;
      }
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleIntroductionUpdate = (section) => (update) => {
  const parseResult = parseDeclaration(update.raw);
  let declaration;

  if (parseResult.status && parseResult.value.type === "term") {
    declaration = parseResult.value;
  } else if (update.raw === "") {
    declaration = {
      type: "empty",
    };
  } else {
    declaration = {
      type: "invalid",
    };
  }

  declaration.raw = update.raw;

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number) {
      switch (step.type) {
        case "introduction-with-assumption":
        case "introduction":
          return {
            ...step,
            status: undefined,
            key: nextKey(update.number),
            term: declaration.name,
            termType: declaration.termType,
            declarationRaw: update.raw,
            declarationType: declaration.type,
          };

        default: return step;
      }
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleNumberUpdate = (section, addMessage) => (update) => {
  // Fails if the number already exists
  let alreadyExist = false;
  mapSteps(section.steps, (step) =>
    alreadyExist |= step.number === update.newNumber
  );
  if (alreadyExist) {
    addMessage({
      type: "Error",
      title: "Step number already exists",
      message: `The step number ${update.newNumber} already exists.`
    });
    return;
  }

  const updatedSteps = replaceStepNumber(
    section.steps,
    update.number, 
    update.newNumber
  );

  return {
    ...section,
    steps: updatedSteps,
  };
};

const handleJustificationUpdate = (section) => (update) => {
  let justification = update.justificationRaw.trim();
  if (justification.toLowerCase() === "given") {
    justification = { type: "given" };
  } else if (justification === "?") {
    justification = { type: "?" };
  } else {
    justification = justification.split(" ").filter((j) => j.length > 0);
  }

  const updatedSteps = mapSteps(section.steps, (step) => {
    if (step.number === update.number) {
      return {
        status: undefined,
        key: nextKey(update.number),
        type: step.type,
        number: step.number,
        formula: step.formula,
        raw: step.raw,
        justification: justification,
      };
    } else {
      return step;
    }
  });

  return {
    ...section,
    steps: updatedSteps,
  };
};

class Program extends Component {
  constructor(props) {
    super(props);
    this.hiddenUpload = React.createRef();

    const sectionsWithKeys = props.sections.map((section) => ({
      ...section,
      key: nextKey("SECTION:"),
      show: true,
    }));

    const processedSections = sectionsWithKeys.map(initialiseSection);

    this.onDragEnd = this.onDragEnd.bind(this);

    this.uploadFile = this.uploadFile.bind(this);
    
    this.section = this.section.bind(this);

    this.renderSteps = this.renderSteps.bind(this);
    this.renderSection = this.renderSection.bind(this);

    this.state = {
      sections: processedSections,
    }
  }

  onDragEnd(result) {
    if (result.destination === null) {
      return;
    }

    const movedSection = this.state.sections[result.source.index];
    const deletedSections = [
      ...this.state.sections.slice(0, result.source.index),
      ...this.state.sections.slice(result.source.index + 1)
    ];

    const updatedSections = [
      ...deletedSections.slice(0, result.destination.index),
      movedSection,
      ...deletedSections.slice(result.destination.index)
    ];

    this.setState({
      sections: updatedSections,
    });
  }

  uploadFile(event) {
    const reader = new FileReader();
    reader.onload = (event) => {
      const raw = event.target.result;
      const program = parseProgram(raw);
      this.setState({
        sections: program.map((section) => ({
          ...section,
          show: true,
          key: nextKey("SECTION:"),
        })).map(initialiseSection),
      });
    };
    if (event.target.files.length > 0) {
      reader.readAsText(event.target.files[0]);
    } else {
      this.props.addMessageHandler({
        type: "Info",
        title: "No file selected",
        message: "Please select a file to open.",
      });
    }
  }

  // Do something in a section
  section(sectionKey) { return (action) => (...args) => {
    const updatedSections = this.state.sections.map((section) => {
      if (section.key === sectionKey) {
        return {
          ...action(section, this.props.addMessageHandler)(...args),
          result: undefined,
          //key: nextKey("SECTION:"),
        };
      } else {
        return section;
      }
    });

    this.setState({
      sections: updatedSections,
    });
  } }

  addSection(type) {
    let newSection;
    switch (type) {
      case "comment":
        newSection = {
          type: "comment",
          text: "",
        };
        break;

      case "proof":
        newSection = {
          type: "proof",
          goalRaw: "",
          goal: "Empty",
          steps: [ {
            key: nextKey("1"),
            type: "step",
            number: "1",
            raw: "P",
            formula: { type: "proposition", name: "P" },
            justification: { type: "?" },
          } ],
        };
        break;

      case "lemmas":
        newSection = {
          type: "lemmas",
          lemmas: [],
        };
        break

      case "declare":
        newSection = {
          type: "declare",
          declarations: [],
        };
        break

      case "define":
        newSection = {
          type: "define",
          definitions: [],
        };
        break

      default: throw new Error("Unknown section type");        
    }

    newSection.key = nextKey("SECTION:");
    newSection.show = true;

    this.setState({
      sections: [...this.state.sections, newSection],
    });
  }

  deleteSection(ident) {
    this.setState({
      sections: this.state.sections.filter(
        (section) => section.key !== ident
      ),
    });
  }

  toggleSection(ident) {
    this.setState({
      sections: this.state.sections.map((section) => {
        if (section.key === ident) {
          return {
            ...section,
            show: !section.show,
          };
        } else {
          return section;
        }
      }),
    });
  }

  exportProof() {
    const print = (e) => Array.isArray(e) ?
      "(" + e.map(print).join(" ") + ")" : e;
    const formatted = this.state.sections.map(exportSection)
      .map(print)
      .join("\n");
    const blob = new Blob([formatted], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "proof.prf");
  }

  checkProof() {
    const proof = this.state.sections;

    const message = validateProgram(proof);

    if (message !== "Ok") {
      this.props.addMessageHandler(message);
      return;
    }

    this.setState({
      checking: true,
    });
    this.resetStepStatus();

    fetch(process.env.REACT_APP_SERVER_URL, {
      method: "POST",
      body: JSON.stringify({ program: proof }),
      headers:{
        'Content-Type': 'application/json'
      }
    }).catch((err) => {
      this.props.addMessageHandler({
        type: "Error",
        title: "An error occured when making request",
        message: err.toString(),
      })
      return false;
    }).then((res) => {
      if (!res) {
        return false;
      }
      switch (res.status) {
        case 200:
          return res.json();

        case 500:
          throw new Error("The server encountered a problem.");

        default:
          throw new Error("An unknown error occured.");
      }
    }).then((result) => {
      if (!result) {
        return false;
      }
      this.updateSectionStatus(result);
    }).catch((err) => {
      this.props.addMessageHandler({
        type: "Error",
        title: "There is an error in the response",
        message: err.message,
      });
    }).finally(() => {
      this.setState({
        checking: false,
      });
    });
  }

  hideAll() {
    this.setState({
      sections: this.state.sections.map((section) => ({
        ...section,
        show: false,
      })),
    });
  }

  showAll() {
    this.setState({
      sections: this.state.sections.map((section) => ({
        ...section,
        show: true,
      })),
    });
  }

  resetStepStatus() {
    const updateStep = (step) => ({
      ...step,
      status: undefined,
    });

    const updatedSections = this.state.sections.map((section) => {
      if (section.type === "proof") {
        return {
          ...section,
          steps: mapSteps(section.steps, updateStep),
        };
      } else {
        return section;
      }
    });

    this.setState({
      sections: updatedSections,
    });
  }

  updateSectionStatus(result) {
    let next = -1;
    const updateStep = (step) => {
      const code = result[next].steps.find((result) =>
        result.number === step.number
      );
      if (code === undefined) {
        return step;
      } else {
        return {
          ...step,
          status: code.result,
        };
      }
    };

    const updatedSections = this.state.sections.map((section) => {
      if (section.type === "comment") {
        return section;
      }
      ++next;
      if (next >= result.length) {
        return section;
      }
      if (section.type === "proof" && result[next].result !== "Error") {
        const updatedSteps = mapSteps(section.steps, updateStep);
        return {
          ...section,
          steps: updatedSteps,
          result: result[next].result,
        };
      } else {
        return {
          ...section,
          result: result[next].result,
        };
      }
    });

    this.setState({
      sections: updatedSections,
    });
  }

  renderDeclarations(declarations, sectionKey) {
    const section = this.section(sectionKey);
    return declarations.map((dec) => (
      <Declaration
        key={dec.key}
        ident={dec.key}

        addMessageHandler={this.props.addMessageHandler}

        deleteHandler={section(handleGenericDelete(dec.key))}
        updateHandler={section(handleDeclarationUpdate)}

        type={dec.type}
        name={dec.name}
        termType={dec.termType}
        argumentTypes={dec.argumentTypes}
        returnType={dec.returnType}
        cases={dec.cases}
        raw={dec.raw}
      />
    ));
  }

  renderLemmas(lemmas, sectionKey) {
    const section = this.section(sectionKey);
    return lemmas.map((lem) => (
      <Lemma
        key={lem.key}
        ident={lem.key}
        
        addMessageHandler={this.props.addMessageHandler}

        deleteHandler={section(handleGenericDelete(lem.key))}
        updateHandler={section(handleLemmaUpdate)}

        number={lem.number}
        formula={lem.formula}
        raw={formatFormula(lem.formula)}
      />
    ));
  }

  renderDefinitions(definitions, sectionKey) {
    const section = this.section(sectionKey);
    return definitions.map((def) => (
      <Definition
        key={def.key}
        ident={def.key}
        
        addMessageHandler={this.props.addMessageHandler}

        deleteHandler={section(handleGenericDelete(def.key))}
        updateHandler={section(handleDefinitionUpdate)}

        number={def.number}
        lhs={def.lhs}
        rhs={def.rhs}
        raw={def.raw}
      />
    ));
  }

  renderSteps(steps, sectionKey) {
    const inductiveTypes = this.state.sections.map((section) =>
      section.type === "declare" ?
        section.declarations.filter((dec) => dec.type === "data")
          .map((dec) => dec.name)
        :
        []
    ).flat();
    const declarations = this.state.sections.map((section) =>
      section.type === "declare" ? section.declarations : []
    ).flat();
    return steps.map((step) => {
      const section = this.section(sectionKey);
      return(
      <Step
        key={step.key}

        inductiveTypes={inductiveTypes}

        addMessageHandler={this.props.addMessageHandler}

        deleteHandler={section(handleStepDelete)}
        addAfterHandler={section(handleStepAddAfter)}

        updateHandler={section(handleStepUpdate(declarations))}

        type={step.type}
        status={step.status === undefined ? "" : step.status}
        number={step.number}

        formula={step.formula}
        assumption={step.assumption}
        subproof={step.subproof}
        raw={step.raw}

        term={step.term}
        termType={step.termType}
        declarationRaw={step.declarationRaw}
        declarationType={step.declarationType}
        cases={step.cases}
        definition={step.definition}
        equalities={step.equalities}

        justification={step.justification}
      />) }
    );
  }

  renderSection(section) {
    switch (section.type) {
      case "comment": return(
        <div key={section.key} className="block">
          <Comment
            updateHandler={this.section(section.key)(handleCommentUpdate)}
            text={section.text}
          />
        </div>
      );

      case "declare": return(
        <div key={section.key} className="block">
          {this.renderDeclarations(section.declarations, section.key)}
          <button
            onClick={this.section(section.key)(addDeclaration)}>
            ADD
          </button>
          <br/>
          <span>
            The integer type (Int) and atom type (Atom)
            are already defined for you.
          </span>
        </div>
      );

      case "lemmas": return(
        <div key={section.key} className="block">
          <table className="steps">
            <tbody>
              {this.renderLemmas(section.lemmas, section.key)}
            </tbody>
          </table>
          <button
            onClick={this.section(section.key)(addLemma)}>
            ADD
          </button>
        </div>
      );

      case "define": return(
        <div key={section.key} className="block">
          <table className="steps">
            <tbody>
              {this.renderDefinitions(section.definitions, section.key)}
            </tbody>
          </table>
          <button
            onClick={this.section(section.key)(addDefinition)}>
            ADD
          </button>
        </div>
      );

      case "proof": return(
        <div key={section.key} className="block">
          { section.result === "Ok" ? (
            <span className="proof-status-ok">Checked: Correct</span>
          ) : section.result === "No" ? (
            <span className="proof-status-no">Checked: Incorrect</span>
          ) : (null)}
          <br/>
          <span>
            <span className="goal-text">
              Goal:
            </span>
            <Formula
              updateHandler={this.section(section.key)(handleGoalUpdate)}
              formula={section.goal}
              raw={section.goalRaw}
            />
          </span>
          <table className="steps">
            <tbody>
              {this.renderSteps(section.steps, section.key)}
            </tbody>
          </table>
          <button
            onClick={this.section(section.key)(renumberSteps)}
          >
            Renumber
          </button>
        </div>
      );

      default: throw new Error("Bad section");
    }
  }

  render() {
    const sectionTitle = (type) => {
      switch (type) {
        case "comment": return "Comment";
        case "proof": return "Proof";
        case "declare": return "Declare";
        case "define": return "Define";
        case "lemmas": return "Lemmas";
        default: return "[UNKNOWN]";
      }
    };

    return (
      <div className="proof">
        <input
          ref={this.hiddenUpload}
          title="Open a file"
          type="file"
          name="uploader"
          style={ { display: "none" } }
          onChange={this.uploadFile}
        />
        <div className="toolbar">
          <h1>Emmy</h1>
          <button
            disabled={this.state.checking}
            title={this.state.checking ?
              "Checking the proof, please wait..." : "Check the proof" }
            onClick={() => this.checkProof()}
            style={ { float: "right", marginRight: "10%" } }
          >
            {this.state.checking ?
              "CHECKING..." : "CHECK" }
          </button>
          <button
            title="Open a file on your computer"
            onClick={() => this.hiddenUpload.current.click()}
          >
            Open
          </button>
          <button
            title="Save the program to a file"
            onClick={() => this.exportProof()}
          >
            Save
          </button>
          <button
            onClick={() => this.hideAll()}>
            HIDE ALL
          </button>
          <button
            onClick={() => this.showAll()}>
            SHOW ALL
          </button>
        </div>
        <div className="proof-inner">
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                >
                  {this.state.sections.map((section, index) => (
                    <Draggable
                      key={section.key}
                      draggableId={section.key}
                      index={index}
                    >
                      {(provided) => (
                        <div 
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                        >
                          <span
                            className={section.result === "Error" ?
                                        "block-header-error" :
                                        "block-header"}
                            {...provided.dragHandleProps}
                          >
                            { section.result === "Error" ? "Error: " : "" }
                            { sectionTitle(section.type) }
                          </span>
                          <button
                            className="section-action"
                            onClick={() => this.toggleSection(section.key)}>
                            {section.show ? "HIDE" : "SHOW"}
                          </button>
                          <button
                            className="section-action"
                            onClick={() => this.deleteSection(section.key)}>
                            X
                          </button>
                          <div
                            style={{display:section.show ? "block" : "none"}}
                          >
                            <MathJax.Provider>
                              {this.renderSection(section)}
                            </MathJax.Provider>
                          </div>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
          {this.state.sections.length === 0 ? (<>
            <span>
              The program is now empty.
              Use the below buttons to add new sections to the program!
            </span>
            <br/>
          </>) : (null)}
          <button
            onClick={() => this.addSection("comment")}>
            ADD COMMENT
          </button>
          <button
            onClick={() => this.addSection("proof")}>
            ADD PROOF
          </button>
          <button
            onClick={() => this.addSection("declare")}>
            ADD DECLARE
          </button>
          <button
            onClick={() => this.addSection("lemmas")}>
            ADD LEMMAS
          </button>
          <button
            onClick={() => this.addSection("define")}>
            ADD DEFINE
          </button>
        </div>
      </div>
    )
  }
}

export default Program;

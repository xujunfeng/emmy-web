import React from 'react';
import './App.css';

const typeToClassName = (type) => {
  switch (type) {
    case "Error": return "message-error";
    case "Info": return "message-info";
    default: return "";
  }
}

function Message(props) {
  return (
    <div className={typeToClassName(props.type)}>
      <button
        onClick={() => props.dismissHandler(props.id)}
      >
        x
      </button>
      <span className="message-title">{props.title}</span>
      <p className="message-body">{props.message}</p>
    </div>
  );
}

export default Message

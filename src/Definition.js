import React, { Component } from 'react';
import './App.css';

import { formatDefinition } from './util/format.js';
//import Formula from './Formula.js';

const STEP_NUMBER_REGEX = /^[A-Za-z0-9-]+$/;

class Definition extends Component {
  constructor(props) {
    super(props);

    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleNumberKeyPress = this.handleNumberKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.finishEdit = this.finishEdit.bind(this);

    this.numberInput = React.createRef();
    this.textArea = React.createRef();

    this.state = {
      number: props.number,
      raw: props.raw,

      editNumber: false,
      edit: false,
    };
  }

  handleNumberChange(event) {
    this.setState({
      number: event.target.value,
    });
  }

  handleNumberKeyPress(event) {
    if ("Enter" === event.key) {
      this.finishEditNumber();
    }
  }

  startEdit() {
    this.setState({
      edit: true,
    }, () => this.textArea.current.focus());
  }

  finishEdit() {
    if (this.state.raw === this.props.raw) {
      this.setState({
        edit: false
      });
    } else {
      this.props.updateHandler({
        ident: this.props.ident,
        raw: this.state.raw,
      });
    }
  }

  handleChange(event) {
    this.setState({
      raw: event.target.value
    });
  }

  handleKeyPress(event) {
    if (event.ctrlKey && "Enter" === event.key) {
      this.finishEdit();
    }
  }

  startEditNumber() {
    this.setState({
      editNumber: true,
    }, () => this.numberInput.current.focus());
  }

  finishEditNumber() {
    if (this.state.number === this.props.number) {
      this.setState({
        editNumber: false,
      });
      return;
    }

    if (this.state.number === "") {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number cannot be empty",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    if (this.state.number.match(STEP_NUMBER_REGEX) === null) {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number can only contain english letters, digits, and `-'.",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    this.props.updateHandler({
      ident: this.props.ident,
      newNumber: this.state.number,
    });
  }

  renderNumber() {
    if (this.state.editNumber) {
      return (
        <input
          ref={this.numberInput}
          onChange={this.handleNumberChange}
          onKeyPress={this.handleNumberKeyPress}
          onBlur={() => this.finishEditNumber()}
          type="text"
          value={this.state.number}
        />
      )
    } else {
      return (
        <span
          onClick={() => this.startEditNumber()}
          className="step-number"
        >
          {this.props.number}
        </span>
      )
    }
  }

  renderDefinitionBody() {
    if (this.state.edit) {
      return (
        <>
          <button
            onClick={this.finishEdit}
          >
            Done
          </button>
          <textarea
            ref={this.textArea}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            onBlur={() => this.finishEdit()}
            rows={(this.state.raw.match(/\n/g) || []).length + 1}
            cols={60}
            value={this.state.raw}
          />
        </>
      );
    } else {
      return (
        <pre>
          <code
            className="declaration-text"
            onClick={() => this.startEdit()}
          >
            {formatDefinition({ lhs: this.props.lhs, rhs: this.props.rhs })}
          </code>
        </pre>
      );
    }
  }

  render() {
    return (
      <tr className={ this.props.status === "No" ? "error-step" : "step"}>
        <td className="step-delete-cell">
          <button
            onClick={() => this.props.deleteHandler(this.props.ident)}
          >
          x
          </button>
        </td>
        <td className="step-number-cell">
          {this.renderNumber()}
        </td>
        <td className="step-formula-cell">
          {this.renderDefinitionBody()}
        </td>
      </tr>
    );
  }
}

export default Definition;

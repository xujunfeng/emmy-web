// Returns a program in s-expression

const exportTerm = (term) => {
  switch (term.type) {
    case "atom": return term.name;
    case "integer": return term.value.toString();
    case "application": return [
      term.function,
      ...term.arguments.map(exportTerm)
    ];
    case "arithmetic": return [
      term.operator,
      ...term.arguments.map(exportTerm)
    ];

    default: throw new Error("Bad term");
  }
};

const exportFormula = (fml) => {
  switch (fml.type) {
    case "proposition": return fml.name;
    case "top": return "Top";
    case "bottom": return "Bottom";

    case "function-induction": return [
      "Ind:",
      fml.definition,
      ...fml.subformulae.map(exportFormula)
    ];

    case "data-induction": return [
      "Ind:",
      ...fml.subformulae.map(exportFormula)
    ];

    case "unary":
    case "binary": return [
      fml.connective,
      ...fml.subformulae.map(exportFormula)
    ];

    case "quantification": return [
      fml.quantifier,
      fml.termType,
      fml.term,
      ...fml.subformulae.map(exportFormula),
    ];

    case "predicate": return [
      fml.predicate,
      ...fml.arguments.map(exportTerm),
    ];

    case "comparison": return [
      fml.operator,
      ...fml.arguments.map(exportTerm),
    ];

    default: throw new Error("Bad formula");
  }
};

const exportStep = (step) => {
  const exportJustification = (just) => {
    if (Array.isArray(just)) {
      return just;
    } else {
      switch (just.type) {
        case "given": return "Given";
        default: return just.type;
      }
    }
  };

  const exportEquality = (equality) => [
    "=",
    exportTerm(equality.term),
    exportJustification(equality.justification),
  ];

  const exportPremise = (p) => p.assumption === undefined ? [
    "Take", p.termType, p.term,
  ] : [
    "Take", p.termType, p.term,
    "such", "that", exportFormula(p.assumption),
  ];

  const exportCase = (c) => [
    ...c.premises.map(exportPremise).flat(),
    "Show", exportFormula(c.goal),
    ...c.subproof.map(exportStep),
  ];

  switch (step.type) {
    case "step": return [
      step.number,
      exportFormula(step.formula),
      exportJustification(step.justification),
    ];

    case "equalities": return [
      step.number,
      "=",
      exportTerm(step.term),
      ...step.equalities.map(exportEquality).flat(),
    ];

    case "data-induction": return [
      step.number,
      "Induction",
      exportFormula(step.formula),
      ...step.cases.map(exportCase),
    ];

    case "function-induction": return [
      step.number,
      "Induction", "on", step.definition,
      exportFormula(step.formula),
      ...step.cases.map(exportCase),
    ];

    case "assumption": return [
      step.number,
      "Assume",
      exportFormula(step.assumption),
      ...step.subproof.map(exportStep),
    ];

    case "introduction": return [
      step.number,
      "Take",
      step.termType,
      step.term,
      ...step.subproof.map(exportStep),
    ];

    case "introduction-with-assumption": return [
      step.number,
      "Take",
      step.termType,
      step.term,
      "Such", "that",
      exportFormula(step.assumption),
      ...step.subproof.map(exportStep),
    ];

    default: throw new Error("Bad step");
  }
};

const exportDeclaration = (dec) => {
  switch (dec.type) {
    case "type": return [ "Type", dec.name ];
    case "term": return [ "Term", dec.termType, dec.name ];

    case "function": return [
      "Function", dec.name,
      ...dec.argumentTypes,
      "->",
      dec.returnType
    ];
 
    case "predicate": return [
      "Predicate", dec.name,
      ...dec.argumentTypes,
    ];

    case "data": return [
      "Data", dec.name,
      ...dec.cases.map((c) => [ c.name, ...c.argumentTypes ])
    ];

    default: throw new Error("Bad declaration");
  }
};

const exportDefinition = (def) => {
  const exportRhsCase = (c) => [
    (c.condition === "otherwise" ? "otherwise" : exportFormula(c.condition)),
    exportTerm(c.body),
  ];

  return [
    def.number,
    exportTerm(def.lhs),
    ...(Array.isArray(def.rhs) ?
      [ "cases", ...def.rhs.map(exportRhsCase) ]:
      [ exportTerm(def.rhs) ] ),
  ];
}

const exportLemma = (lemma) => [
  lemma.number,
  exportFormula(lemma.formula),
];

const exportSection = (section) => {
  switch (section.type) {
    case "comment": return "\"" + section.text + "\"";

    case "proof": return [
      "Proof",
      exportFormula(section.goal),
      ...section.steps.map(exportStep),
    ];

    case "define": return [
      "Define",
      ...section.definitions.map(exportDefinition),
    ];

    case "declare": return [
      "Declare",
      ...section.declarations.map(exportDeclaration),
    ];

    case "lemmas": return [
      "Lemma",
      ...section.lemmas.map(exportLemma),
    ];

    default: throw new Error("Bad section");
  }
};

export default exportSection;

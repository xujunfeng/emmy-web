// Provides formatting utilities

const formatTerm = (term) => {
  switch (term.type) {
    case "atom": return term.name;
    case "integer": return term.value.toString();
    case "application":
      const args = term.arguments.map((arg) => {
        switch (arg.type) {
          case "application":
          case "arithmetic":
            return "(" + formatTerm(arg) + ")";
          default:
            return formatTerm(arg);
        }
      });
      return term.function + " " + args.join(" ");
    case "arithmetic":
      const leftRaw = term.arguments[0];
      const leftBracket =
        (term.operator === "*" || term.operator === "/") &&
        (leftRaw.operator === "+" || leftRaw.operator === "-");
      const left = leftBracket ?
        "(" + formatTerm(leftRaw) + ")" :
        formatTerm(leftRaw);
      const rightRaw = term.arguments[1];
      const right = rightRaw.type === "arithmetic" ?
        "(" + formatTerm(rightRaw) + ")" :
        formatTerm(rightRaw);
      return left + " " + term.operator + " " + right;

    default: return " ";
  }
};

const formatConnective = (conn) => {
  return conn;
}

const formatFormula = (fml) => {
  switch (fml.type) {
    case "proposition":
      return fml.name;
    case "top":
      return "Top";
    case "bottom":
      return "Bottom";

    case "function-induction":
      return "Ind:[ "
        + fml.definition
        + " "
        + formatFormula(fml.subformulae[0])
        + "]";

    case "data-induction":
      return "Ind:[ "
        + formatFormula(fml.subformulae[0])
        + "]";

    case "unary":
      return formatConnective(fml.connective)
        + " "
        + (fml.subformulae[0].type === "binary" ? " ( " : " ")
        + formatFormula(fml.subformulae[0])
        + (fml.subformulae[0].type === "binary" ? " ) " : " ");

    case "binary":
      return formatFormula(fml.subformulae[0])
        + " "
        + formatConnective(fml.connective)
        + " "
        + formatFormula(fml.subformulae[1]);

    case "quantification":
      return fml.quantifier
        + " "
        + fml.term
        + ": "
        + fml.termType
        + ". "
        + (fml.subformulae[0].type === "binary" ? "[" : "")
        + formatFormula(fml.subformulae[0])
        + (fml.subformulae[0].type === "binary" ? "]" : "");

    case "predicate":
      return fml.predicate
        + "("
        + fml.arguments.map(formatTerm).join(", ")
        + ") "

    case "comparison":
      return formatTerm(fml.arguments[0])
        + " "
        + fml.operator
        + " "
        + formatTerm(fml.arguments[1]);

    default:
      return "[Malformed formula]";
  }
};

const formatDeclaration = (dec) => {
  switch (dec.type) {
    case "type": return "Type " + dec.name;
    case "term": return dec.name + ": " + dec.termType;
    case "function": return dec.name
      + ": "
      + dec.argumentTypes.join(" ")
      + " -> "
      + dec.returnType;
    case "predicate": return dec.name
      + ": "
      + dec.argumentTypes.join(" ")
      + " -> Prop";
    case "data": return "Data "
      + dec.name
      + " = "
      + dec.cases.map((c) => c.name + " " + c.argumentTypes.join(" "))
        .join(" | ")
    default: throw new Error("Bad declaration");
  }
};

const formatDefinition = (def, multiline) => {
  const formatRhsCase = (c) =>
      (c.condition === "otherwise" ? "otherwise" : formatFormula(c.condition))
      + " = "
      + formatTerm(c.body);

  return formatTerm(def.lhs)
    + (Array.isArray(def.rhs) ?
       "\n  | " + def.rhs.map(formatRhsCase).join("\n  | ") :
       " = " + formatTerm(def.rhs));
}

export {
  formatTerm,
  formatFormula,
  formatDeclaration,
  formatDefinition,
};

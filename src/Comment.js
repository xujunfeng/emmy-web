import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';

class Comment extends Component {
  constructor(props) {
    
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.finishEdit = this.finishEdit.bind(this);

    this.textArea = React.createRef();

    this.state = {
      edit: false,
      text: props.text,
    }
  }

  startEdit() {
    this.setState({
      edit: true,
    }, () => this.textArea.current.focus());
  }

  finishEdit() {
    if (this.state.text === this.props.text) {
      this.setState({
        edit: false
      });
    } else {
      this.props.updateHandler({
        text: this.state.text
      });
    }
  }

  handleChange(event) {
    this.setState({
      text: event.target.value
    });
  }

  handleKeyPress(event) {
    if (event.ctrlKey && "Enter" === event.key) {
      this.finishEdit();
    }
  }

  render() {
    if (this.state.edit) {
      return (
        <>
          <button
            onClick={this.finishEdit}
          >
            Done
          </button>
          <textarea
            ref={this.textArea}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            onBlur={() => this.finishEdit()}
            rows={(this.state.text.match(/\n/g) || []).length + 1}
            cols={60}
            value={this.state.text}
          />
        </>
      );
    } else {
      return(
        <div className="comment-block">
          <ReactMarkdown source={this.props.text}/>
          <button onClick={() => this.startEdit()}>
            EDIT
          </button>
        </div>
      );
    }
  }
}

export default Comment;

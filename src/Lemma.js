import React, { Component } from 'react';
import './App.css';

import Formula from './Formula.js';

const STEP_NUMBER_REGEX = /^[A-Za-z0-9-]+$/;

class Lemma extends Component {
  constructor(props) {
    super(props);

    this.handleFormulaUpdate = this.handleFormulaUpdate.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleNumberKeyPress = this.handleNumberKeyPress.bind(this);

    this.numberInput = React.createRef();

    this.state = {
      number: props.number,

      editNumber: false,
    };
  }

  handleNumberChange(event) {
    this.setState({
      number: event.target.value,
    });
  }

  handleNumberKeyPress(event) {
    if ("Enter" === event.key) {
      this.finishEditNumber();
    }
  }

  handleFormulaUpdate(raw) {
    this.props.updateHandler({
      ident: this.props.ident,
      raw: raw
    })
  }

  startEditNumber() {
    this.setState({
      editNumber: true,
    }, () => this.numberInput.current.focus());
  }

  finishEditNumber() {
    if (this.state.number === this.props.number) {
      this.setState({
        editNumber: false,
      });
      return;
    }

    if (this.state.number === "") {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number cannot be empty",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    if (this.state.number.match(STEP_NUMBER_REGEX) === null) {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number can only contain english letters, digits, and `-'.",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    this.props.updateHandler({
      ident: this.props.ident,
      newNumber: this.state.number,
    });
  }

  renderNumber() {
    if (this.state.editNumber) {
      return (
        <input
          ref={this.numberInput}
          onChange={this.handleNumberChange}
          onKeyPress={this.handleNumberKeyPress}
          onBlur={() => this.finishEditNumber()}
          type="text"
          value={this.state.number}
        />
      )
    } else {
      return (
        <span
          onClick={() => this.startEditNumber()}
          className="step-number"
        >
          {this.props.number}
        </span>
      )
    }
  }

  render() {
    return (
      <tr className={ this.props.status === "No" ? "error-step" : "step"}>
        <td className="step-delete-cell">
          <button
            onClick={() => this.props.deleteHandler(this.props.ident)}
          >
          x
          </button>
        </td>
        <td className="step-number-cell">
          {this.renderNumber()}
        </td>
        <td className="step-formula-cell">
          <Formula
            formula={this.props.formula}
            raw={this.props.raw}

            updateHandler={this.handleFormulaUpdate}
          />
        </td>
      </tr>
    );
  }
}

export default Lemma;

import React, { Component } from 'react';
//import MathJax from 'react-mathjax';

import { formatDeclaration } from './util/format.js';

class Declaration extends Component {
  constructor(props) {
    
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.finishEdit = this.finishEdit.bind(this);

    this.textArea = React.createRef();

    this.state = {
      edit: false,
      raw: props.raw,
    }
  }

  startEdit() {
    this.setState({
      edit: true,
    }, () => this.textArea.current.focus());
  }

  finishEdit() {
    if (this.state.raw === this.props.raw) {
      this.setState({
        edit: false
      });
    } else {
      this.props.updateHandler({
        ident: this.props.ident,
        raw: this.state.raw
      });
    }
  }

  handleChange(event) {
    this.setState({
      raw: event.target.value
    });
  }

  handleKeyPress(event) {
    if (event.ctrlKey && "Enter" === event.key) {
      this.finishEdit();
    }
  }

  renderDeclaration() {
    switch (this.props.type) {
      case "empty": return (
        <span>
          Click to edit declaration!
        </span>
      );
      case "invalid": return (
        <span className="error-text">
          Invalid declaration
        </span>
      );
      case "type":
      case "term":
      case "function":
      case "predicate":
      case "data":
        return (
          <span className="declaration-text">
            {formatDeclaration(this.props)}
          </span>
        );

      default: throw new Error("Bad declaration");
    }
  }

  renderDeleteButton() {
    if (this.props.deleteHandler === undefined) {
      return "";
    } else {
      return (
        <button
          className="declaration-button"
          onClick={() => this.props.deleteHandler(this.props.ident) }
        >
        x
        </button>
      );
    }
  }

  render() {
    if (this.state.edit) {
      return (
        <>
          <button
            onClick={this.finishEdit}
          >
            Done
          </button>
          <textarea
            ref={this.textArea}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            onBlur={() => this.finishEdit()}
            rows={(this.state.raw.match(/\n/g) || []).length + 1}
            cols={60}
            value={this.state.raw}
          />
        </>
      );
    } else {
      return(
        <div 
          className="declaration-block"
        >
          { this.renderDeleteButton() }
          <span
            onClick={() => this.startEdit()}
          >
          { this.renderDeclaration() }
          </span>
        </div>
      );
    }
  }
}

export default Declaration;

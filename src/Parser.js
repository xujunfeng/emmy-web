import Parsimmon from 'parsimmon';

const fixFormula = (fml) => {
  switch (fml.type) {
    case "proposition":
    case "predicate":
    case "comparison":
    case "top":
    case "bottom":
      return fml;

    case "unary":
    case "binary":
    case "quantification":
    case "binary-fragment":
      return {
        ...fml,
        subformulae: fml.subformulae.map(fixFormula)
      }

    case "raw-binary":
      return fixBinaryFormula(fml);

    case "data-induction":
    case "function-induction":
      return {
        ...fml,
        subformulae: fml.subformulae.map(fixFormula)
      };

    default:
      throw new Error("Unknown formula");
  }
};

const fixBinaryFormula = (fml) => {
  const connective = fml.connective;
  const left = fml.subformulae[0];
  const right = fml.subformulae[1];

  if (right.type === "binary-fragment") {
    return {
      type: "binary",
      connective: connective,
      subformulae: [
        fixFormula(left),
        fixFormula(right.subformulae[0])
      ]
    };
  } else if (right.type === "binary") {
    return fixBinaryFormula({
      type: "raw-binary",
      connective: connective,
      subformulae: [
        {
          type: "binary",
          connective: right.connective,
          subformulae: [
            left,
            fixFormula(right.subformulae[0])
          ]
        },
        right.subformulae[1]
      ]
    });
  } else {
    return fml;
  }
};

const fixTerm = (term) => {
  switch (term.type) {
    case "arithmetic-fragment":
      throw new Error("Oh No: " + term.op + " " + term.argument.type);
    case "atom":
    case "integer":
      return term;

    case "application":
      return {
        type: term.type,
        function: term.function,
        arguments: term.arguments.map(fixTerm)
      }

    case "arithmetic":
      return {
        type: term.type,
        operator: term.operator,
        arguments: term.arguments.map(fixTerm)
      }

    case "raw-arithmetic":
      return fixBinaryTerm(term);

    default:
      throw new Error("Unknown term");
  }
};

const fixBinaryTerm = (term) => {
  const operator = term.operator;
  const left = term.arguments[0];
  const right = term.arguments[1];

  if (right.type !== "arithmetic") {
    return {
      type: "arithmetic",
      operator: operator,
      arguments: [
        fixTerm(left),
        fixTerm(right)
      ]
    };
  } else {
    return fixBinaryTerm({
      type: "raw-arithmetic",
      operator: operator,
      arguments: [
        {
          type: "arithmetic",
          operator: right.operator,
          arguments: [
            left,
            fixTerm(right.arguments[0])
          ]
        },
        right.arguments[1]
      ]
    });
  }
};

const TermLang = Parsimmon.createLanguage({

  AtomicTerm: () =>
    Parsimmon.regexp(/[a-z][A-Za-z0-9-]*/)
      .map((name) => ({ type: "atom", name: name })),

  IntegerTerm: () =>
    Parsimmon.regexp(/[+-]?[0-9]+/)
      .map((num) => ({ type: "integer", value: parseInt(num) })),

  SingleTerm: (r) =>
    Parsimmon.alt(
      r.BracketTerm,
      r.AtomicTerm,
      r.IntegerTerm,
    ),

  FunctionApplication: (r) =>
    Parsimmon.seqMap(
      r.AtomicTerm,
      Parsimmon.whitespace,
      r.SingleTerm.sepBy1(Parsimmon.whitespace),
      (func, _0, args) => ({
        type: "application",
        function: func.name,
        arguments: args,
      })
    ),

  BracketTerm: (r) => 
    Parsimmon.seqMap(
      Parsimmon.string("("),
      Parsimmon.optWhitespace,
      r.Sum,
      Parsimmon.optWhitespace,
      Parsimmon.string(")"),
      (_0, _1, formula, _2, _3) => formula
    ),

  ProductLeft: (r) => 
    Parsimmon.alt(
      r.FunctionApplication,
      r.SingleTerm,
    ),

  ProductRight: (r) => 
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.optWhitespace,
        Parsimmon.oneOf("*/"),
        Parsimmon.optWhitespace,
        r.ProductLeft,
        r.ProductRight,
        (_0, op, _2, left, right) => {
          if (right === undefined) {
            return {
              type: "arithmetic-fragment",
              operator: op,
              argument: left,
            };
          } else if (right.type === "arithmetic-fragment") {
            return {
              type: "arithmetic",
              operator: op,
              arguments: [ left, right.argument ]
            };
          } else {
            return {
              type: "arithmetic",
              operator: op,
              arguments: [ left, right ]
            };
          }
        }
      ),
      Parsimmon.succeed(),
    ),

  Product: (r) =>
    Parsimmon.seqMap(
      r.ProductLeft,
      r.ProductRight,
      (left, right) => {
        if (right === undefined) {
          return left;
        } else if (right.type === "arithmetic-fragment") {
          return {
            type: "arithmetic",
            operator: right.operator,
            arguments: [ left, right.argument ]
          };
        } else {
          return {
            type: "raw-arithmetic",
            operator: right.operator,
            arguments: [ left, right ]
          };
        }
      }
    ),

  SumLeft: (r) => r.Product,

  SumRight: (r) => 
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.optWhitespace,
        Parsimmon.oneOf("+-"),
        Parsimmon.optWhitespace,
        r.Product,
        r.SumRight,
        (_0, op, _2, left, right) => {
          if (right === undefined) {
            return {
              type: "arithmetic-fragment",
              operator: op,
              argument: left,
            };
          } else if (right.type === "arithmetic-fragment") {
            return {
              type: "arithmetic",
              operator: op,
              arguments: [ left, right.argument ]
            };
          } else {
            return {
              type: "arithmetic",
              operator: op,
              arguments: [ left, right ]
            };
          }
        }
      ),
      Parsimmon.succeed(),
    ),

  Sum: (r) =>
    Parsimmon.seqMap(
      r.SumLeft,
      r.SumRight,
      (left, right) => {
        if (right === undefined) {
          return left;
        } else if (right.type === "arithmetic-fragment") {
          return {
            type: "arithmetic",
            operator: right.operator,
            arguments: [ left, right.argument ]
          };
        } else {
          return {
            type: "raw-arithmetic",
            operator: right.operator,
            arguments: [ left, right ]
          };
        }
      }
    ),

  Term: (r) => r.Sum.map(fixTerm),

});

const FormulaLang = Parsimmon.createLanguage({
  Proposition: () =>
    Parsimmon.regexp(/[A-Z][A-Za-z0-9-]*/)
      .map((name) => ({ type: "proposition", name: name })),

  FunctionInduction: (r) =>
    Parsimmon.seqMap(
      Parsimmon.string("Ind:"),
      Parsimmon.optWhitespace,
      Parsimmon.string("["),
      Parsimmon.optWhitespace,
      Parsimmon.regexp(/[A-Za-z0-9-]+/),
      Parsimmon.whitespace,
      r.Formula,
      Parsimmon.optWhitespace,
      Parsimmon.string("]"),
      (_0, _1, _2, _3, def, _4, fml, _5, _6) => ({
        type: "function-induction",
        definition: def,
        subformulae: [fml],
      })
    ),

  DataInduction: (r) =>
    Parsimmon.seqMap(
      Parsimmon.string("Ind:"),
      Parsimmon.optWhitespace,
      Parsimmon.string("["),
      Parsimmon.optWhitespace,
      r.Formula,
      Parsimmon.optWhitespace,
      Parsimmon.string("]"),
      (_0, _1, _2, _3, fml, _4, _5) => ({
        type: "data-induction",
        subformulae: [fml],
      })
    ),

  Predicate: (r) =>
    Parsimmon.seqMap(
      r.Proposition,
      Parsimmon.optWhitespace,
      Parsimmon.string("("),
      Parsimmon.optWhitespace,
      TermLang.Term.sepBy1(Parsimmon.seq(
        Parsimmon.optWhitespace,
        Parsimmon.string(","),
        Parsimmon.optWhitespace
      )),
      Parsimmon.optWhitespace,
      Parsimmon.string(")"),
      (pred, _0, _1, _2, args, _3, _4) => ({
        type: "predicate",
        predicate: pred.name,
        arguments: args,
      })
    ),

  Comparison: (r) =>
    Parsimmon.seqMap(
      TermLang.Term,
      Parsimmon.optWhitespace,
      Parsimmon.alt(
        Parsimmon.string("<="),
        Parsimmon.string(">="),
        Parsimmon.oneOf("=<>"),
      ),
      Parsimmon.optWhitespace,
      TermLang.Term,
      (left, _0, comp, _1, right) => ({
        type: "comparison",
        operator: comp,
        arguments: [left, right]
      })
    ),

  Negation: (r) =>
    Parsimmon.seqMap(
      Parsimmon.alt(
        Parsimmon.regexp(/Not/i),
        Parsimmon.oneOf("~¬!")
      ),
      Parsimmon.whitespace,
      r.SingleFormula,
      (_0, _1, fml) => ({
        type: "unary",
        connective: "Not",
        subformulae: [ fml ]
      })
    ),

  Quantification: (r) => 
    Parsimmon.seqMap(
      Parsimmon.alt(
        Parsimmon.regexp(/Forall/i).map(() => "Forall"),
        Parsimmon.regexp(/Exists/i).map(() => "Exists")
      ),
      Parsimmon.whitespace,
      TermLang.AtomicTerm,
      Parsimmon.optWhitespace,
      Parsimmon.string(":"),
      Parsimmon.optWhitespace,
      r.Proposition,
      Parsimmon.optWhitespace,
      Parsimmon.string("."),
      Parsimmon.optWhitespace,
      r.SingleFormula,
      (quant, _0, term, _1, _2, _3, type, _4, _5, _6, fml) => ({
        type: "quantification",
        quantifier: quant,
        term: term.name,
        termType: type.name,
        subformulae: [fml]
      })
    ),
      

  Top: () =>
    Parsimmon.regexp(/Top/i)
      .map((_) => ({ type: "top" })),

  Bottom: () =>
    Parsimmon.regexp(/Bottom/i)
      .map((_) => ({ type: "bottom" })),

  SingleFormula: (r) =>
    Parsimmon.alt(
      r.FunctionInduction,
      r.DataInduction,
      r.BracketFormula,
      r.Predicate,
      r.Comparison,
      r.Quantification,
      r.Top,
      r.Bottom,
      r.Negation,
      r.Proposition
    ),

  ConjunctionLeft: (r) => r.SingleFormula,

  ConjunctionRight: (r) => 
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.whitespace,
        Parsimmon.alt(
          Parsimmon.regexp(/And/i),
          Parsimmon.string("/\\")
        ),
        Parsimmon.whitespace,
        r.SingleFormula,
        r.ConjunctionRight,
        (_0, _1, _2, left, right) => {
          if (right === undefined) {
            return {
              type: "binary-fragment",
              connective: "And",
              subformulae: [ left ]
            };
          } else {
            return {
              type: "binary",
              connective: "And",
              subformulae: [ left, right ]
            };
          }
        }
      ),
      Parsimmon.succeed(),
    ),

  Conjunction: (r) =>
    Parsimmon.seqMap(
      r.ConjunctionLeft,
      r.ConjunctionRight,
      (left, right) => {
        if (right === undefined) {
          return left;
        } else {
          return {
            type: "raw-binary",
            connective: "And",
            subformulae: [ left, right ]
          };
        }
      }
    ),
  
  DisjunctionLeft: (r) => r.Conjunction,

  DisjunctionRight: (r) => 
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.whitespace,
        Parsimmon.alt(
          Parsimmon.regexp(/Or/i),
          Parsimmon.string("\\/")
        ),
        Parsimmon.whitespace,
        r.Conjunction,
        r.DisjunctionRight,
        (_0, _1, _2, left, right) => {
          if (right === undefined) {
            return {
              type: "binary-fragment",
              connective: "Or",
              subformulae: [ left ]
            };
          } else {
            return {
              type: "binary",
              connective: "Or",
              subformulae: [ left, right ]
            };
          }
        }
      ),
      Parsimmon.succeed(),
    ),

  Disjunction: (r) =>
    Parsimmon.seqMap(
      r.DisjunctionLeft,
      r.DisjunctionRight,
      (left, right) => {
        if (right === undefined) {
          return left;
        } else {
          return {
            type: "raw-binary",
            connective: "Or",
            subformulae: [ left, right ]
          };
        }
      }
    ),

  Implication: (r) =>
    Parsimmon.alt(
      Parsimmon.seqMap(
        r.Disjunction,
        Parsimmon.whitespace,
        Parsimmon.alt(
          Parsimmon.regexp(/Implies/i),
          Parsimmon.string("->"),
        ),
        Parsimmon.whitespace,
        r.Implication,
        (left, _0, _1, _2, right) => ({
          type: "binary",
          connective: "Implies",
          subformulae: [ left, right ]
        })
      ),
      r.Disjunction
    ),

  Equivalence: (r) =>
    Parsimmon.alt(
      Parsimmon.seqMap(
        r.Implication,
        Parsimmon.whitespace,
        Parsimmon.regexp(/if-and-only-if/i),
        Parsimmon.whitespace,
        r.Equivalence,
        (left, _0, _1, _2, right) => ({
          type: "binary",
          connective: "If-and-only-if",
          subformulae: [ left, right ]
        })
      ),
      r.Implication
    ),

  BracketFormula: (r) => 
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.string("("),
        Parsimmon.optWhitespace,
        r.Equivalence,
        Parsimmon.optWhitespace,
        Parsimmon.string(")"),
        (_0, _1, formula, _2, _3) => formula
      ),
      Parsimmon.seqMap(
        Parsimmon.string("["),
        Parsimmon.optWhitespace,
        r.Equivalence,
        Parsimmon.optWhitespace,
        Parsimmon.string("]"),
        (_0, _1, formula, _2, _3) => formula
      )
    ).map(fixFormula),

  Formula: (r) => r.Equivalence.map(fixFormula),

});

const DeclarationLang = Parsimmon.createLanguage({

  Type: () =>
    Parsimmon.seqMap(
      Parsimmon.string("Type"),
      Parsimmon.whitespace,
      FormulaLang.Proposition,
      (_0, _1, prop) => ({
        type: "type",
        name: prop.name,
      })
    ),

  Term: () =>
    Parsimmon.seqMap(
      TermLang.AtomicTerm,
      Parsimmon.optWhitespace,
      Parsimmon.string(":"),
      Parsimmon.optWhitespace,
      FormulaLang.Proposition,
      (term, _0, _1, _2, prop) => ({
        type: "term",
        name: term.name,
        termType: prop.name,
      })
    ),

  ArgumentTypes: () =>
    FormulaLang.Proposition
      .map((prop) => prop.name)
      .sepBy1(Parsimmon.whitespace),

  Function: (r) =>
    Parsimmon.seqMap(
      TermLang.AtomicTerm,
      Parsimmon.optWhitespace,
      Parsimmon.string(":"),
      Parsimmon.optWhitespace,
      r.ArgumentTypes,
      Parsimmon.optWhitespace,
      Parsimmon.string("->"),
      Parsimmon.optWhitespace,
      FormulaLang.Proposition,
      (term, _0, _1, _2, argTypes, _3, _4, _5, retType) => ({
        type: "function",
        name: term.name,
        argumentTypes: argTypes,
        returnType: retType.name,
      })
    ),

  Predicate: (r) =>
    Parsimmon.seqMap(
      FormulaLang.Proposition,
      Parsimmon.optWhitespace,
      Parsimmon.string(":"),
      Parsimmon.optWhitespace,
      r.ArgumentTypes,
      Parsimmon.optWhitespace,
      Parsimmon.string("->"),
      Parsimmon.optWhitespace,
      Parsimmon.string("Prop"),
      (term, _0, _1, _2, argTypes, _3, _4, _5, _6) => ({
        type: "predicate",
        name: term.name,
        argumentTypes: argTypes,
      })
    ),

  Case: (r) =>
    Parsimmon.alt(
      Parsimmon.seqMap(
        TermLang.AtomicTerm,
        Parsimmon.whitespace,
        FormulaLang.Proposition
          .map((prop) => prop.name)
          .sepBy(Parsimmon.whitespace),
        (term, _0, argTypes) => ({
          name: term.name,
          argumentTypes: argTypes
        })
      ),
      TermLang.AtomicTerm.map((term) => ({
        name: term.name,
        argumentTypes: [],
      }))
    ),

  Data: (r) =>
    Parsimmon.seqMap(
      Parsimmon.string("Data"),
      Parsimmon.optWhitespace,
      FormulaLang.Proposition,
      Parsimmon.optWhitespace,
      Parsimmon.string("="),
      Parsimmon.optWhitespace,
      r.Case.sepBy1(Parsimmon.seq(
        Parsimmon.optWhitespace,
        Parsimmon.string("|"),
        Parsimmon.optWhitespace
      )),
      (_0, _1, prop, _2, _3, _4, cases) => ({
        type: "data",
        name: prop.name,
        cases: cases
      })
    ),

  Declaration: (r) =>
    Parsimmon.alt(
      r.Type,
      r.Data,
      r.Predicate,
      r.Function,
      r.Term,
    ),

});

const DefinitionLang = Parsimmon.createLanguage({

  Condition: () =>
    Parsimmon.alt(
      Parsimmon.string("otherwise"),
      FormulaLang.Formula,
    ),

  Case: (r) =>
    Parsimmon.seqMap(
      r.Condition,
      Parsimmon.optWhitespace,
      Parsimmon.string("="),
      Parsimmon.optWhitespace,
      TermLang.Term,
      (cond, _0, _1, _2, body) => ({
        condition: cond,
        body: body,
      })
    ),

  Rhs: (r) =>
    Parsimmon.alt(
      Parsimmon.seqMap(
        Parsimmon.string("="),
        Parsimmon.optWhitespace,
        TermLang.Term,
        (_0, _1, body) => body
      ),
      Parsimmon.seqMap(
        Parsimmon.optWhitespace,
        Parsimmon.string("|"),
        Parsimmon.optWhitespace,
        r.Case,
        (_0, _1, _2, c) => c
      ).many(),
    ),

  Definition: (r) =>
    Parsimmon.seqMap(
      TermLang.FunctionApplication,
      Parsimmon.optWhitespace,
      r.Rhs,
      (lhs, _0, rhs) => ({
        type: "definition",
        lhs: lhs,
        rhs: rhs,
      })
    ),

});

const parseFormula = (str) => {
  return FormulaLang.Formula.parse(str.trim());
};
const parseTerm = (str) => {
  return TermLang.Term.parse(str.trim());
};
const parseDeclaration = (str) => {
  return DeclarationLang.Declaration.parse(str.trim());
};
const parseDefinition = (str) => {
  return DefinitionLang.Definition.parse(str.trim());
};

export {
  parseTerm,
  parseFormula,
  parseDeclaration,
  parseDefinition
};

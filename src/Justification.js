import React, { Component } from 'react';
import './App.css';

class Justification extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.input = React.createRef();

    let raw = props.justification;
    if (Array.isArray(raw)) {
      raw = raw.join(' ');
    } else {
      raw = raw.type;
    }

    this.state = {
      number: props.number,
      raw: raw,
      originalRaw: raw,

      edit: false,
    };
  }

  handleChange(event) {
    this.setState({
      raw: event.target.value,
    });
  }

  handleKeyPress(event) {
    if ("Enter" === event.key) {
      this.finishEdit();
    }
  }

  startEdit() {
    this.setState({
      edit: true,
    }, () => this.input.current.focus());
  }

  finishEdit() {
    if (this.state.raw === this.state.originalRaw) {
      this.setState({
        edit: false,
      });
    } else {
      this.props.updateHandler(this.state.raw);
    }
  }

  render() {
    if (this.state.edit) {
      return (
        <input
          ref={this.input}
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          type="text"
          value={this.state.raw}
          onBlur={() => this.finishEdit()}
        />
      )
    }

    if (Array.isArray(this.props.justification)) {
      if (this.props.justification.length === 0) {
        return (
          <span
            onClick={() => this.startEdit()}
            className="justification-empty"
          >
            No justification
          </span>
        );
      } else {
        return this.props.justification.map((num) =>
          <span
            onClick={() => this.startEdit()}
            className= {num[num.length - 1] === ":" ? "rule-name" : "step-number"}
            key={num}
          >
            {num}
          </span>
        );
      }
    } else if ("given" === this.props.justification.type) {
      return (
        <span
          onClick={() => this.startEdit()}
          className="justification-given"
        >
          Given
        </span>
      );
    } else if ("?" === this.props.justification.type) {
      return (
        <span
          onClick={() => this.startEdit()}
          className="justification-question"
        >
          ?
        </span>
      );
    }
  }
}

export default Justification;

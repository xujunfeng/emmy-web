import {
  parseTerm,
  parseFormula,
  parseDeclaration,
} from './Parser.js'

it('parses propositions', () => {
  expect(parseFormula("A"))
    .toEqual({ status: true, value: { type: "proposition", name: "A" } });
  expect(parseFormula("P"))
    .toEqual({ status: true, value: { type: "proposition", name: "P" } });
  expect(parseFormula("Abc123-456---XYZ"))
    .toEqual({
      status: true,
      value: { type: "proposition", name: "Abc123-456---XYZ" }
    });
});

it('parses tops and bottoms', () => {
  expect(parseFormula("Top"))
    .toEqual({ status: true, value: { type: "top" } });
  expect(parseFormula("top"))
    .toEqual({ status: true, value: { type: "top" } });
  expect(parseFormula("Bottom"))
    .toEqual({ status: true, value: { type: "bottom" } });
  expect(parseFormula("BOTTOM"))
    .toEqual({ status: true, value: { type: "bottom" } });
  expect(parseFormula("BoTtOm"))
    .toEqual({ status: true, value: { type: "bottom" } });
});

it('parses unary formulae', () => {
  expect(parseFormula("NOt X"))
    .toEqual({
      status: true,
      value: {
        type: "unary",
        connective: "Not",
        subformulae: [ { type: "proposition", name: "X" } ]
      },
    });
  expect(parseFormula("NOT Top"))
    .toEqual({
      status: true,
      value: {
        type: "unary",
        connective: "Not",
        subformulae: [ { type: "top" } ]
      },
    });
  expect(parseFormula("Not not NOT Bottom"))
    .toEqual({
      status: true,
      value: {
        type: "unary", connective: "Not", subformulae: [
          { type: "unary", connective: "Not", subformulae: [
            { type: "unary", connective: "Not", subformulae: [
              { type: "bottom" } ] } ] } ] }
    });
});

it('parses binary formulae', () => {
  expect(parseFormula("X and Y"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "And",
        subformulae: [
          { type: "proposition", name: "X" },
          { type: "proposition", name: "Y" },
        ],
      },
    });

  expect(parseFormula("X ImPlIes Bottom"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "Implies",
        subformulae: [
          { type: "proposition", name: "X" },
          { type: "bottom" },
        ],
      },
    });

  expect(parseFormula("not X OR not Y"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "Or",
        subformulae: [
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "X" } ] },
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "Y" } ] },
        ],
      },
    });

  expect(parseFormula("Not X /\\ not Y"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "And",
        subformulae: [
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "X" } ] },
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "Y" } ] },
        ],
      },
    });

  expect(parseFormula("! X \\/ ¬ Y"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "Or",
        subformulae: [
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "X" } ] },
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "Y" } ] },
        ],
      },
    });

  expect(parseFormula("~ X -> ¬ Y"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "Implies",
        subformulae: [
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "X" } ] },
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "Y" } ] },
        ],
      },
    });
});

it('parses quantifiers', () => {
  expect(parseFormula("forall x: Int. x = 0"))
    .toEqual({
      status: true,
      value: {
        type: "quantification",
        quantifier: "Forall",
        term: "x",
        termType: "Int",
        subformulae: [
          { type: "comparison", operator: "=", arguments: [
            { type: "atom", name: "x" },
            { type: "integer", value: 0 } ] } ] }
    });

  expect(parseFormula("ExiSTS xyz: Atom. [P(xyz) or X]"))
    .toEqual({
      status: true,
      value: {
        type: "quantification",
        quantifier: "Exists",
        term: "xyz",
        termType: "Atom",
        subformulae: [
          { type: "binary", connective: "Or", subformulae: [
            { type: "predicate", predicate: "P", arguments: [
              { type: "atom", name: "xyz" } ] },
            { type: "proposition", name: "X" } ] } ] }
    });
});

it('handles precedence', () => {
  expect(parseFormula("A and B or C Implies not D and E or F and not G"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Implies", subformulae: [
          { type: "binary", connective: "Or", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "A" },
              { type: "proposition", name: "B" } ] },
            { type: "proposition", name: "C" } ] },
          { type: "binary", connective: "Or", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "unary", connective: "Not", subformulae: [
                { type: "proposition", name: "D" } ] },
              { type: "proposition", name: "E" } ] },
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "F" },
              { type: "unary", connective: "Not", subformulae: [
                { type: "proposition", name: "G" } ] } ] } ] } ] }
    });
});

it('handles brackets', () => {
  expect(parseFormula("(X and Y)"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "And",
        subformulae: [
          { type: "proposition", name: "X" },
          { type: "proposition", name: "Y" },
        ],
      },
    });

  expect(parseFormula("[(X) and (Not [Y])]"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "And",
        subformulae: [
          { type: "proposition", name: "X" },
          { type: "unary", connective: "Not", subformulae: [
            { type: "proposition", name: "Y" } ] },
        ],
      },
    });

  expect(parseFormula("(A or B) and C"))
    .toEqual({
      status: true,
      value: {
        type: "binary",
        connective: "And",
        subformulae: [
          { type: "binary", connective: "Or", subformulae: [
            { type: "proposition", name: "A" },
            { type: "proposition", name: "B" } ] },
          { type: "proposition", name: "C" }
        ],
      },
    });

  expect(parseFormula("A and (B or C) Implies [not (D and E) or F] and not G"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Implies", subformulae: [
          { type: "binary", connective: "And", subformulae: [
            { type: "proposition", name: "A" },
            { type: "binary", connective: "Or", subformulae: [
              { type: "proposition", name: "B" },
              { type: "proposition", name: "C" } ] } ] },
          { type: "binary", connective: "And", subformulae: [
            { type: "binary", connective: "Or", subformulae: [
              { type: "unary", connective: "Not", subformulae: [
                { type: "binary", connective: "And", subformulae: [
                  { type: "proposition", name: "D" },
                  { type: "proposition", name: "E" } ] } ] },
              { type: "proposition", name: "F" } ] },
            { type: "unary", connective: "Not", subformulae: [
              { type: "proposition", name: "G" } ] } ] } ] }
    });

  expect(parseFormula("(A or B) and (C implies D)"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "And", subformulae: [
          { type: "binary", connective: "Or", subformulae: [
            { type: "proposition", name: "A" },
            { type: "proposition", name: "B" } ] },
          { type: "binary", connective: "Implies", subformulae: [
            { type: "proposition", name: "C" },
            { type: "proposition", name: "D" } ] } ] },
    });
});

it('handles associativity', () => {
  expect(parseFormula("A implies B implies C"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Implies", subformulae: [
        { type: "proposition", name: "A" },
        { type: "binary", connective: "Implies", subformulae: [
          { type: "proposition", name: "B" },
          { type: "proposition", name: "C" } ] } ] },
    });

  expect(parseFormula("A and B and C"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "And", subformulae: [
        { type: "binary", connective: "And", subformulae: [
          { type: "proposition", name: "A" },
          { type: "proposition", name: "B" } ] },
        { type: "proposition", name: "C" } ] }
    });

  expect(parseFormula("A or B or C or D"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Or", subformulae: [
        { type: "binary", connective: "Or", subformulae: [
          { type: "binary", connective: "Or", subformulae: [
            { type: "proposition", name: "A" },
            { type: "proposition", name: "B" } ] },
          { type: "proposition", name: "C" } ] },
        { type: "proposition", name: "D" } ] }
    });

  expect(parseFormula("A and B and C or D and E and F or G and H and I"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Or", subformulae: [
        { type: "binary", connective: "Or", subformulae: [
          { type: "binary", connective: "And", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "A" },
              { type: "proposition", name: "B" } ] },
            { type: "proposition", name: "C" } ] },
          { type: "binary", connective: "And", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "D" },
              { type: "proposition", name: "E" } ] },
            { type: "proposition", name: "F" } ] } ] },
        { type: "binary", connective: "And", subformulae: [
          { type: "binary", connective: "And", subformulae: [
            { type: "proposition", name: "G" },
            { type: "proposition", name: "H" } ] },
          { type: "proposition", name: "I" } ] } ] }
    });

  expect(parseFormula("A and B and C implies D and E and F implies G and H and I"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "Implies", subformulae: [
        { type: "binary", connective: "And", subformulae: [
          { type: "binary", connective: "And", subformulae: [
            { type: "proposition", name: "A" },
            { type: "proposition", name: "B" } ] },
          { type: "proposition", name: "C" } ] },
        { type: "binary", connective: "Implies", subformulae: [
          { type: "binary", connective: "And", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "D" },
              { type: "proposition", name: "E" } ] },
            { type: "proposition", name: "F" } ] },
          { type: "binary", connective: "And", subformulae: [
            { type: "binary", connective: "And", subformulae: [
              { type: "proposition", name: "G" },
              { type: "proposition", name: "H" } ] },
            { type: "proposition", name: "I" } ] } ] } ] },
    });

});

it('parses atomic terms', () => {
  expect(parseTerm("a"))
    .toEqual({ status: true, value: { type: "atom", name: "a" } });
  expect(parseTerm("abcd"))
    .toEqual({ status: true, value: { type: "atom", name: "abcd" } });
  expect(parseTerm("abc123-456---XYZ"))
    .toEqual({
      status: true,
      value: { type: "atom", name: "abc123-456---XYZ" }
    });
});

it('parses integers', () => {
  expect(parseTerm("100"))
    .toEqual({ status: true, value: { type: "integer", value: 100 } });
  expect(parseTerm("0"))
    .toEqual({ status: true, value: { type: "integer", value: 0 } });
  expect(parseTerm("-100"))
    .toEqual({ status: true, value: { type: "integer", value: -100 } });
});

it('parses function applications', () => {
  expect(parseTerm("f x y z"))
    .toEqual({
      status: true,
      value: { type: "application", function: "f", arguments: [
        { type: "atom", name: "x" },
        { type: "atom", name: "y" },
        { type: "atom", name: "z" } ] },
    });
  expect(parseTerm("f (g 1 2) -10 x (h 1 2)"))
    .toEqual({
      status: true,
      value: { type: "application", function: "f", arguments: [
        { type: "application", function: "g", arguments: [
          { type: "integer", value: 1 },
          { type: "integer", value: 2 } ] },
        { type: "integer", value: -10 },
        { type: "atom", name: "x" },
        { type: "application", function: "h", arguments: [
          { type: "integer", value: 1 },
          { type: "integer", value: 2 } ] } ] }
    });
});

it('parses arithmetic operations', () => {
  expect(parseTerm("1 + 1"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "integer", value: 1 },
        { type: "integer", value: 1 } ] }
    });

  expect(parseTerm("1 * 10 * 100"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "*", arguments: [
        { type: "arithmetic", operator: "*", arguments: [
          { type: "integer", value: 1 },
          { type: "integer", value: 10 } ] },
        { type: "integer", value: 100 } ] }
    });

  expect(parseTerm("1 * 10 + 2 * 100"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "arithmetic", operator: "*", arguments: [
          { type: "integer", value: 1 },
          { type: "integer", value: 10 } ] },
        { type: "arithmetic", operator: "*", arguments: [
          { type: "integer", value: 2 },
          { type: "integer", value: 100 } ] } ] }
    });

  expect(parseTerm("1 * (10 + 11) + -1 * 100"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "arithmetic", operator: "*", arguments: [
          { type: "integer", value: 1 },
          { type: "arithmetic", operator: "+", arguments: [
            { type: "integer", value: 10 },
            { type: "integer", value: 11 } ] } ] },
        { type: "arithmetic", operator: "*", arguments: [
          { type: "integer", value: -1 },
          { type: "integer", value: 100 } ] } ] }
    });

  expect(parseTerm("a * (b + 100) + c / (d - 200)"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "arithmetic", operator: "*", arguments: [
          { type: "atom", name: "a" },
          { type: "arithmetic", operator: "+", arguments: [
            { type: "atom", name: "b" },
            { type: "integer", value: 100 } ] } ] },
        { type: "arithmetic", operator: "/", arguments: [
          { type: "atom", name: "c" },
          { type: "arithmetic", operator: "-", arguments: [
            { type: "atom", name: "d" },
            { type: "integer", value: 200 } ] } ] } ] }
    });

  expect(parseTerm("f 10 + g (a + h -100 * c)"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "application", function: "f", arguments: [
          { type: "integer", value: 10 } ] },
        { type: "application", function: "g", arguments: [
          { type: "arithmetic", operator: "+", arguments: [
            { type: "atom", name: "a" },
              { type: "arithmetic", operator: "*", arguments: [
                { type: "application", function: "h", arguments: [
                  { type: "integer", value: -100 } ] },
                { type: "atom", name: "c" } ] } ] } ] } ] },
    });

  expect(parseTerm("1 + 1 - -20 + -100"))
    .toEqual({
      status: true,
      value: { type: "arithmetic", operator: "+", arguments: [
        { type: "arithmetic", operator: "-", arguments: [
          { type: "arithmetic", operator: "+", arguments: [
            { type: "integer", value: 1 },
            { type: "integer", value: 1 } ] },
          { type: "integer", value: -20 } ] },
        { type: "integer", value: -100 } ] },
    });
});

it('parses predicate applications', () => {
  expect(parseFormula("P(x, y z)"))
    .toEqual({
      status: true,
      value: { type: "predicate", predicate: "P", arguments: [
        { type: "atom", name: "x" },
        { type: "application", function: "y", arguments: [
          { type: "atom", name: "z" } ] } ] },
    });

  // Ambiguous, since +3 coule be a term or operation `+ 3'
  expect(parseFormula("Q   (  1  , f    2 +3  ,a*(  1 + 2  )        )"))
    .toEqual({
      status: true,
      value: { type: "predicate", predicate: "Q", arguments: [
        { type: "integer", value: 1 },
        { type: "application", function: "f", arguments: [
          { type: "integer", value: 2 },
          { type: "integer", value: 3 } ] },
        { type: "arithmetic", operator: "*", arguments: [
          { type: "atom", name: "a" },
          { type: "arithmetic", operator: "+", arguments: [
            { type: "integer", value: 1 },
            { type: "integer", value: 2 } ] } ] } ] }
    });

  expect(parseFormula("Q   (  1  , f    2 + 3  ,a*(  1 + 2  )        )"))
    .toEqual({
      status: true,
      value: { type: "predicate", predicate: "Q", arguments: [
        { type: "integer", value: 1 },
        { type: "arithmetic", operator: "+", arguments: [
          { type: "application", function: "f", arguments: [
            { type: "integer", value: 2 } ] },
          { type: "integer", value: 3 } ] },
        { type: "arithmetic", operator: "*", arguments: [
          { type: "atom", name: "a" },
          { type: "arithmetic", operator: "+", arguments: [
            { type: "integer", value: 1 },
            { type: "integer", value: 2 } ] } ] } ] }
    });
});

it('parses comparisons', () => {
  expect(parseFormula("x > y z"))
    .toEqual({
      status: true,
      value: { type: "comparison", operator: ">", arguments: [
        { type: "atom", name: "x" },
        { type: "application", function: "y", arguments: [
          { type: "atom", name: "z" } ] } ] },
    });

  expect(parseFormula("f 100 = g 10 And P"))
    .toEqual({
      status: true,
      value: { type: "binary", connective: "And", subformulae: [
        { type: "comparison", operator: "=", arguments: [
          { type: "application", function: "f", arguments: [
            { type: "integer", value: 100 } ] },
          { type: "application", function: "g", arguments: [
            { type: "integer", value: 10 } ] } ] },
        { type: "proposition", name: "P" } ] }
    });
});

it('parses declarations', () => {
  expect(parseDeclaration("Type X"))
    .toEqual({
      status: true,
      value: { type: "type", name: "X" }
    });

  expect(parseDeclaration("Type XYZ-abc---1234ABCD"))
    .toEqual({
      status: true,
      value: { type: "type", name: "XYZ-abc---1234ABCD" }
    });

  expect(parseDeclaration("x : Atom"))
    .toEqual({
      status: true,
      value: { type: "term" , name: "x", termType: "Atom" }
    });

  expect(parseDeclaration("abcd-1234:Int"))
    .toEqual({
      status: true,
      value: { type: "term", name: "abcd-1234", termType: "Int" }
    });

  expect(parseDeclaration("x    :                   Something"))
    .toEqual({
      status: true,
      value: { type: "term", name: "x", termType: "Something" }
    });

  expect(parseDeclaration("f: Int Int Int -> Int"))
    .toEqual({
      status: true,
      value: { type: "function", name: "f", argumentTypes: [
        "Int", "Int", "Int"
      ], returnType: "Int" }
    });
 
  //                                            |
  //                     Whitespace needed here v
  expect(parseDeclaration("f12345   :X       Y Z ->ABC"))
    .toEqual({
      status: true,
      value: { type: "function", name: "f12345", argumentTypes: [
        "X", "Y", "Z"
      ], returnType: "ABC" }
    });

  expect(parseDeclaration("P: Int Int Int -> Prop"))
    .toEqual({
      status: true,
      value: { type: "predicate", name: "P", argumentTypes: [
        "Int", "Int", "Int" ] }
    });

  expect(parseDeclaration("P12345   :X       Y Z ->Prop"))
    .toEqual({
      status: true,
      value: { type: "predicate", name: "P12345", argumentTypes: [
        "X", "Y", "Z" ] }
    });

  expect(parseDeclaration("Data Nat = z\
                                    | s Nat"))
    .toEqual({
      status: true,
      value: { type: "data", name: "Nat", cases: [
        { name: "z", argumentTypes: [] },
        { name: "s", argumentTypes: [ "Nat" ] } ] }
    });

  expect(parseDeclaration("Data Tree = leaf Int\
                                     | flower Int Int\
                                     | branch Tree Int Tree"))
    .toEqual({
      status: true,
      value: { type: "data", name: "Tree", cases: [
        { name: "leaf", argumentTypes: [ "Int" ] },
        { name: "flower", argumentTypes: [ "Int", "Int" ] },
        { name: "branch", argumentTypes: [ "Tree", "Int", "Tree" ] } ] }
    });

});

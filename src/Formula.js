import React, { Component } from 'react';
import MathJax from 'react-mathjax';

import { formatTerm } from './util/format.js';

const precedence = (conn) => {
  switch (conn) {
    case "And": return 9;
    case "Or": return 8;
    case "Implies": return 7;
    case "If-and-only-if": return 6;
    case "Not": return 10
    default: return 0;
  }
};

const translateOperator = (op) => {
  switch (op) {
    case ">=": return "\\geq";
    case "<=": return "\\leq";
    default: return op;
  }
};

const translateTerm = (term) => {
  return "\\color{darkblue}{\\texttt{" + formatTerm(term) + "}}";
};

const translateConnective = (conn) => {
  const translate = (conn) => {
    switch (conn) {
      case "And": return " \\land ";
      case "Or": return " \\lor ";
      case "Implies": return " \\to ";
      case "If-and-only-if": return " \\iff ";
      case "Not": return " \\lnot ";

      default: return " ";
    }
  };
  return "\\color{darkred}{" + translate(conn) + "}";
};


const translateQuantifier = (quant) => {
  const translate = (quant) => {
    switch (quant) {
      case "Forall": return " \\forall ";
      case "Exists": return " \\exists ";
      default: return " ";
    }
  }
  return "\\color{darkred}{" + translate(quant) + "}";
};

const translateFormula = (fml) => {
  switch (fml.type) {
    case "atom":
    case "integer":
    case "application":
    case "arithmetic":
      return translateTerm(fml);

    case "proposition":
      return fml.name;
    case "top":
      return " \\top ";
    case "bottom":
      return " \\bot ";

    case "function-induction":
      return "Ind:[ "
        + fml.definition
        + "~"
        + translateFormula(fml.subformulae[0])
        + "]";

    case "data-induction":
      return "Ind:[ "
        + translateFormula(fml.subformulae[0])
        + "]";

    case "unary":
      return translateConnective(fml.connective)
        + (fml.subformulae[0].type === "binary" ? " ( " : " ")
        + translateFormula(fml.subformulae[0])
        + (fml.subformulae[0].type === "binary" ? " ) " : " ");

    case "quantification":
      return translateQuantifier(fml.quantifier)
        + " "
        + translateTerm({ type: "atom", name: fml.term })
        + ": "
        + "\\color{blue}{\\texttt{" + fml.termType + "}}"
        + "."
        + (fml.subformulae[0].type === "binary" ? " [ " : " ")
        + translateFormula(fml.subformulae[0])
        + (fml.subformulae[0].type === "binary" ? " ] " : " ");

    case "binary":
      const left = fml.subformulae[0];
      const right = fml.subformulae[1];
      const leftBracket = left.type === "binary"
        && precedence(left.connective) < precedence(fml.connective);
      const rightBracket = right.type === "binary"
        && precedence(right.connective) < precedence(fml.connective);

      return (leftBracket ? "(" : "")
        + translateFormula(left)
        + (leftBracket ? ")" : "")
        + "~" + translateConnective(fml.connective) + "~"
        + (rightBracket ? "(" : "")
        + translateFormula(right)
        + (rightBracket ? ")" : "");

    case "comparison":
      return translateTerm(fml.arguments[0])
        + "~"
        + translateOperator(fml.operator)
        + "~"
        + translateTerm(fml.arguments[1]);

    case "predicate":
      return fml.predicate
        + "("
        + fml.arguments.map(translateTerm).join(", ")
        + ")";

    default: return " ";
  }
};

const estimateLength = (fml) => {
  switch (fml.type) {
    case "atom":
    case "integer":
    case "application":
    case "arithmetic":
      return formatTerm(fml).length;

    case "proposition":
      return fml.name.length;

    case "top":
    case "bottom":
      return 1;

    case "function-induction":
      return 4
        + fml.definition.length
        + estimateLength(fml.subformulae[0]);

    case "data-induction":
      return 4
        + estimateLength(fml.subformulae[0]);

    case "unary":
      return 1 + estimateLength(fml.subformulae[0]);

    case "quantification":
      return 5
        + fml.term.length
        + fml.termType.length
        + estimateLength(fml.subformulae[0]);

    case "binary":
      return 1
        + estimateLength(fml.subformulae[0])
        + estimateLength(fml.subformulae[1]);

    case "comparison":
      return 1
        + estimateLength(fml.arguments[0])
        + estimateLength(fml.arguments[1]);

    case "predicate":
      return 1
        + fml.predicate.length
        + fml.arguments.map(estimateLength).reduce((x, y) => x + y);

    default: return 0;
  }
};

// Returns a list of lines
const typesetFormula = (fml, allow) => {
  const INDENT = 5;
  if (allow < 30) {
    allow = 30;
  }
  let subformula;
  let skip;
  switch (fml.type) {
    case "atom":
    case "integer":
    case "application":
    case "arithmetic":
      return [ translateTerm(fml) ];

    case "proposition":
      return [ fml.name ];
    case "top":
      return [ " \\top " ];
    case "bottom":
      return [ " \\bot " ];

    case "function-induction":
    case "data-induction":
      subformula = fml.subformulae[0];
      skip = estimateLength(subformula) < allow;
      return [
        "Ind:",
        "%OPEN[",
        ...(fml.definition === undefined ? [] : [fml.definition + "~"]),
        ...(skip ? ["%SKIP"] : ["%PUSH"]),
        ...typesetFormula(subformula, allow - INDENT),
        ...(skip ? ["%SKIP"] : ["%POP"]),
        "%CLOSE]"
      ];

    case "unary":
      subformula = fml.subformulae[0];
      return [
        translateConnective(fml.connective),
        ...(subformula.type === "binary" ? ["%OPEN(", "%SKIP"] : ["%SKIP"]),
        ...typesetFormula(subformula),
        ...(subformula.type === "binary" ? ["%CLOSE)", "%SKIP"] : ["%SKIP"]),
      ];

    case "quantification":
      subformula = fml.subformulae[0];
      skip = subformula.type === "quantification"
        || estimateLength(subformula) < allow;
      return [
        translateQuantifier(fml.quantifier)
          + " "
          + translateTerm({ type: "atom", name: fml.term })
          + ": "
          + "\\color{blue}{\\texttt{" + fml.termType + "}}"
          + ".",
        ...(subformula.type === "binary" ? ["%OPEN["] : []),
      
        ...(skip ? ["%SKIP"] : ["%PUSH"]),

        ...typesetFormula(fml.subformulae[0], allow - (skip ? 0 : INDENT)),
        
        ...(skip ? ["%SKIP"] : ["%POP"]),

        ...(subformula.type === "binary" ? ["%CLOSE]"] : []),
      ];

    case "binary":
      const left = fml.subformulae[0];
      const right = fml.subformulae[1];
      const leftBracket = left.type === "binary"
        && precedence(left.connective) < precedence(fml.connective);
      const rightBracket = right.type === "binary"
        && precedence(right.connective) < precedence(fml.connective);

      if (estimateLength(fml) > allow) {
        return [
          ...(leftBracket ? ["%OPEN("] : []),
          ...typesetFormula(left, allow - INDENT),
          ...(leftBracket ? ["%CLOSE)"] : []),
          "~~"
            + translateConnective(fml.connective),
          ...(rightBracket ? ["%OPEN("] : []),
          ...typesetFormula(right, allow - INDENT),
          ...(rightBracket ? ["%CLOSE)"] : []),
        ];
      } else {
        return [ translateFormula(fml) ];
      }

    case "comparison":
      if (estimateLength(fml) > allow) {
        return [
          translateTerm(fml.arguments[0]),
          "~~" + translateOperator(fml.operator),
          translateTerm(fml.arguments[1])
        ];
      } else {
        return [
          translateTerm(fml.arguments[0])
            + "~"
            + translateOperator(fml.operator)
            + "~"
            + translateTerm(fml.arguments[1])
        ];
      }

    case "predicate":
      return [
        fml.predicate
          + "("
          + fml.arguments.map(translateTerm).join(",~")
          + ")"
      ];

    default: return [];
  }
};

class Formula extends Component {
  constructor(props) {
    
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.finishEdit = this.finishEdit.bind(this);
    this.textArea = React.createRef();

    this.state = {
      edit: false,
      raw: props.raw,
      mouseOver: false,
    }
  }

  handleCopy(e) {
    navigator.clipboard.writeText(this.props.raw).then(() => {}, () => {});
  }

  handleClick(e) {
    this.startEdit();
  }

  startEdit() {
    this.setState({
      edit: true,
    }, () => this.textArea.current.focus());
  }

  finishEdit() {
    if (this.state.raw === this.props.raw) {
      this.setState({
        edit: false
      });
    } else {
      this.props.updateHandler(this.state.raw);
    }
  }

  handleChange(event) {
    this.setState({
      raw: event.target.value
    });
  }

  handleKeyPress(event) {
    if (event.ctrlKey && "Enter" === event.key) {
      this.finishEdit();
    }
  }

  handleMouseEnter() {
    this.setState({
      mouseOver: true,
    });
  }

  handleMouseOut() {
    this.setState({
      mouseOver: false,
    });
  }

  render() {
    if (this.state.edit) {
      return (
        <>
          <button
            onClick={this.finishEdit}
          >
            Done
          </button>
          <textarea
            ref={this.textArea}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            onBlur={() => this.finishEdit() }
            rows={(this.state.raw.match(/\n/g) || []).length + 1}
            cols={60}
            value={this.state.raw}
          />
        </>
      );
    } else {
      if (this.props.formula === "Invalid") {
        return (
          <span
            className="error-text"
            onClick={() => this.startEdit()}
          >
            Incorrect formula
          </span>
        );
      } else if (this.props.formula === "Empty") {
        return (
          <span
            className="formula-error"
            onClick={() => this.startEdit()}
          >
            Click here to edit formula
          </span>
        );
      } else {
        let levels = 0;
        let next = "";
        let skip = false;
        const sizeModifier = () => {
          switch (size) {
            case 0: return "\\Bigg";
            case 1: return "\\bigg";
            case 2: return "\\Big";
            case 3: return "\\big";
            default: return "";
          }
        };
        const lines = typesetFormula(this.props.formula, 60);
        let size = 4;
        lines.reduce((cont, line) => {
          switch (line) {
            case "%OPEN(":
            case "%OPEN[":
              size = cont - 1 < size ? cont - 1 : size;
              return cont - 1;

            case "%CLOSE)":
            case "%CLOSE]":
              return cont + 1;

            default: return cont;
          }
        }, 4);
        const formatted = "\\begin{align} &"
          + lines.reduce((prev, line) => {
              switch (line) {
                case "%PUSH": ++levels; return prev;
                case "%POP":  --levels; return prev;
                case "%SKIP": skip = true; return prev;

                case "%OPEN(":
                  next = sizeModifier() + "(";
                  ++size;
                  return prev;
                case "%CLOSE)":
                  --size;
                  skip = false;
                  return prev + sizeModifier() + ")";

                case "%OPEN[":
                  ++size;
                  return prev + sizeModifier() + "[";
                case "%CLOSE]":
                  --size;
                  skip = false;
                  return prev + sizeModifier() + "]";

                default:
                  const result = prev
                    + (skip ? "" : " \\\\ & " + "\\quad".repeat(levels)) + " "
                    + next
                    + line;
                  next = "";
                  skip = false;
                  return result;
              }
            })
          + "\\end{align}";
        return (
          <span
            onMouseEnter={() => this.handleMouseEnter()}
            onMouseLeave={() => this.handleMouseOut()}
          >
            <MathJax.Node
              className="formula"
              title={this.props.raw}
              onClick={(e) => this.handleClick(e)}
              formula={formatted}
            />
            {this.state.mouseOver ? (
              <button
                onClick={() => this.handleCopy()}
                title={"Copy `" + this.props.raw + "' to clipboard."}
              >
                Copy
              </button>
            ) : (null)}
            <textarea
              ref={(ta) => this.copyText = ta}
              style={ { display: "none" } }
              value={this.props.raw}
              onChange={(e) => { } }
            />
          </span>
        );
      }
    }
  }
}

export default Formula;

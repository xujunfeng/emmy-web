import React, { Component } from 'react';
import './App.css';

import Program from './Program.js';
import Message from './Message.js';

let nextMessageId = 1;

class App extends Component {
  constructor(props) {
    super(props);

    this.handleDismissMessage = this.handleDismissMessage.bind(this);
    this.handleAddMessage = this.handleAddMessage.bind(this);

    this.state = {
      messages: [],
    }
  }

  handleDismissMessage(id) {
    const updatedMessages = this.state.messages.filter((msg) =>
      msg.id !== id
    );

    this.setState({
      messages: updatedMessages,
    });
  }

  handleAddMessage(msg) {
    msg.id = nextMessageId;
    nextMessageId++;
    const updatedMessages = [msg].concat(this.state.messages);

    this.setState({
      messages: updatedMessages,
    });
  }

  renderMessages() {
    return this.state.messages.map((msg) =>
      <Message
        dismissHandler={this.handleDismissMessage}

        key={msg.id}
        id={msg.id}
        type={msg.type}
        title={msg.title}
        message={msg.message}
      />
    );
  }

  render() {
    return (
      <div className="app">
        <ul>
          {this.renderMessages()}
        </ul>
         <Program
          addMessageHandler={this.handleAddMessage}

          sections={[]}
        />
      </div>
    );
  }
}

export default App;

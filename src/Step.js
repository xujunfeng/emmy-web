import React, { Component } from 'react';
import './App.css';

import MathJax from 'react-mathjax';

import Formula from './Formula.js';
import Declaration from './Declaration.js';
import Justification from './Justification.js';

const STEP_NUMBER_REGEX = /^[A-Za-z0-9-]+$/;

class Step extends Component {
  constructor(props) {
    super(props);

    this.handleFormulaUpdate = this.handleFormulaUpdate.bind(this);
    this.handleIntroductionUpdate = this.handleIntroductionUpdate.bind(this);
    this.handleCaseGoalUpdate = this.handleCaseGoalUpdate.bind(this);
    this.handlePremiseIntroductionUpdate =
      this.handlePremiseIntroductionUpdate.bind(this);
    this.handlePremiseFormulaUpdate =
      this.handlePremiseFormulaUpdate.bind(this);
    this.handleInduction = this.handleInduction.bind(this);
    this.handleAddCase = this.handleAddCase.bind(this);
    this.handleAddEquality = this.handleAddEquality.bind(this);
    this.handleRemoveEquality = this.handleRemoveEquality.bind(this);
    this.handleRemoveCase = this.handleRemoveCase.bind(this);
    this.handleAddPremise = this.handleAddPremise.bind(this);
    this.handleRemovePremise = this.handleRemovePremise.bind(this);
    this.handleCaseAddStep = this.handleCaseAddStep.bind(this);
    this.handleCaseFlip = this.handleCaseFlip.bind(this);

    this.handleDefinitionChange = this.handleDefinitionChange.bind(this);
    this.handleDefinitionKeyPress = this.handleDefinitionKeyPress.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleNumberKeyPress = this.handleNumberKeyPress.bind(this);
    this.handleJustificationUpdate = this.handleJustificationUpdate.bind(this);

    this.definitionInput = React.createRef();
    this.numberInput = React.createRef();

    this.state = {
      number: props.number,
      definition: props.definition,

      editNumber: false,
      editDefinition: false,
    };
  }

  handleDefinitionChange(event) {
    this.setState({
      definition: event.target.value,
    });
  }

  handleDefinitionKeyPress(event) {
    if ("Enter" === event.key) {
      this.finishEditDefinition();
    }
  }

  handleNumberChange(event) {
    this.setState({
      number: event.target.value,
    });
  }

  handleNumberKeyPress(event) {
    if ("Enter" === event.key) {
      this.finishEditNumber();
    }
  }

  handleEqualityTermUpdate(equalityIndex) { return (raw) => {
    this.props.updateHandler({
      type: "equality-term",
      number: this.props.number,
      equalityIndex: equalityIndex,
      raw: raw,
    })
  }; }

  handleEqualityJustificationUpdate(equalityIndex) { return (raw) => {
    this.props.updateHandler({
      type: "equality-justification",
      number: this.props.number,
      equalityIndex: equalityIndex,
      raw: raw,
    })
  }; }

  handleAddEquality() {
    this.props.updateHandler({
      type: "add-equality",
      number: this.props.number,
    })
  }

  handleRemoveEquality(equalityIndex) { return () => {
    this.props.updateHandler({
      type: "remove-equality",
      number: this.props.number,
      equalityIndex: equalityIndex,
    })
  }; }

  handleCaseFlip(caseIndex) { return () => {
    this.props.updateHandler({
      type: "case-flip",
      number: this.props.number,
      caseIndex: caseIndex,
    })
  }; }

  handleCaseGoalUpdate(caseIndex) { return (raw) => {
    this.props.updateHandler({
      type: "case-goal",
      number: this.props.number,
      caseIndex: caseIndex,
      raw: raw,
    })
  }; }

  handlePremiseIntroductionUpdate(caseIndex, premiseIndex) { return (raw) => {
    this.props.updateHandler({
      type: "premise-introduction",
      number: this.props.number,
      caseIndex: caseIndex,
      premiseIndex: premiseIndex,
      raw: raw.raw,
    })
  }; }

  handlePremiseFormulaUpdate(caseIndex, premiseIndex) { return (raw) => {
    this.props.updateHandler({
      type: "premise-formula",
      number: this.props.number,
      caseIndex: caseIndex,
      premiseIndex: premiseIndex,
      raw: raw,
    })
  }; }

  handleFormulaUpdate(raw) {
    this.props.updateHandler({
      number: this.props.number,
      raw: raw
    })
  }

  handleIntroductionUpdate(raw) {
    this.props.updateHandler({
      type: "introduction",
      number: this.props.number,
      raw: raw.raw
    })
  }

  handleInduction() {
    this.props.updateHandler({
      type: "induction",
      number: this.props.number,
    })
  }

  handleAddCase() {
    this.props.updateHandler({
      type: "add-case",
      number: this.props.number,
    })
  }

  handleRemoveCase(caseIndex) { return () =>
    this.props.updateHandler({
      type: "remove-case",
      number: this.props.number,
      caseIndex: caseIndex,
    })
  }

  handleAddPremise(caseIndex, assumption) { return () =>
    this.props.updateHandler({
      type: "add-premise",
      number: this.props.number,
      caseIndex: caseIndex,
      assumption: assumption,
    })
  }

  handleRemovePremise(caseIndex, premiseIndex) { return () =>
    this.props.updateHandler({
      type: "remove-premise",
      number: this.props.number,
      caseIndex: caseIndex,
      premiseIndex: premiseIndex,
    })
  }

  handleCaseAddStep(caseIndex, type) { return () =>
    this.props.updateHandler({
      type: "case-add-step",
      stepType: type,
      number: this.props.number,
      caseIndex: caseIndex,
    })
  }

  startEditNumber() {
    this.setState({
      editNumber: true,
    }, () => this.numberInput.current.focus());
  }

  startEditDefinition() {
    this.setState({
      editDefinition: true,
    }, () => this.definitionInput.current.focus());
  }

  finishEditDefinition() {
    if (this.state.definition === this.props.definition) {
      this.setState({
        editDefinition: false,
      });
      return;
    }

    if (this.state.number === "") {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid function definition number",
        message: "Function definition number cannot be empty",
      });
      this.setState({
        definition: this.props.definition,
        editDefinition: false,
      });
      return;
    }

    this.props.updateHandler({
      type: "induction-definition",
      number: this.props.number,
      newDefinition: this.state.definition,
    });
  }

  finishEditNumber() {
    if (this.state.number === this.props.number) {
      this.setState({
        editNumber: false,
      });
      return;
    }

    if (this.state.number === "") {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number cannot be empty",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    if (this.state.number.match(STEP_NUMBER_REGEX) === null) {
      this.props.addMessageHandler({
        type: "Error",
        title: "Invalid step number",
        message: "Step number can only contain english letters, digits, and `-'.",
      });
      this.setState({
        number: this.props.number,
        editNumber: false,
      });
      return;
    }

    this.props.updateHandler({
      number: this.props.number,
      newNumber: this.state.number,
    });
  }

  handleJustificationUpdate(raw) {
    this.props.updateHandler({
      number: this.props.number,
      justificationRaw: raw,
    });
  }

  handleCaseJustificationUpdate(caseIndex){ return (raw) => {
    this.props.updateHandler({
      type: "case-justification",
      number: this.props.number,
      caseIndex: caseIndex,
      raw: raw,
    });
  }; }

  handleFlipType() {
    this.props.updateHandler({
      number: this.props.number,
      type: "flip",
      to: this.props.type === "data-induction" ?
        "function-induction" : "data-induction",
    });
  }

  renderNumber() {
    if (this.state.editNumber) {
      return (
        <input
          ref={this.numberInput}
          onChange={this.handleNumberChange}
          onKeyPress={this.handleNumberKeyPress}
          onBlur={() => this.finishEditNumber()}
          type="text"
          value={this.state.number}
        />
      )
    } else {
      return (
        <span
          onClick={() => this.startEditNumber()}
          className="step-number"
        >
          {this.props.number}
        </span>
      )
    }
  }

  renderJustification() {
    return (
      <Justification
        justification={this.props.justification}

        updateHandler={this.handleJustificationUpdate}
      />
    );
  }

  renderInductionButton() {
    if (this.props.type === "step"
        && this.props.justification.type !== "given"
        && this.props.formula.type === "quantification"
        && this.props.formula.quantifier === "Forall"
        && this.props.inductiveTypes.includes(this.props.formula.termType)) {
      return (
        <button
          title="Turn this step into an induction step and generate the induction principle"
          onClick={this.handleInduction}
        >
          I!
        </button>
      );
    } else {
      return (null);
    }
  }

  renderDeleteButton() {
    return (
      <button
        title="Delete this step"
        onClick={() => this.props.deleteHandler(this.props.number)}
      >
        X
      </button>
    );
  }

  renderActionButtons(subproof) {
    return subproof ? (
      <>
        <button
          title="Add a step to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "step")
          }
        >
        +I
        </button>
        <button
          title="Add an assumption step to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "assumption")
          }
        >
        A+I
        </button>
        <button
          title="Add an introduction step to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "introduction")
          }
        >
        I+I
        </button>
        <button
          title="Add an introduction step with an assumption to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "introduction-with-assumption")
          }
        >
        IA+I
        </button>
        <button
          title="Add an induction step to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "data-induction")
          }
        >
        Ind+I
        </button>
        <button
          title="Add an equalities step to the beginning of the subproof of this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, true, "equalities")
          }
        >
        E+I
        </button>
      </>
    ) : (
      <>
        <button
          title="Add a step after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "step")
          }
        >
        +
        </button>
        <button
          title="Add an assumption step after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "assumption")
          }
        >
        A+
        </button>
        <button
          title="Add an introduction step after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "introduction")
          }
        >
        I+
        </button>
        <button
          title="Add an introduction step with an assumption after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "introduction-with-assumption")
          }
        >
        IA+
        </button>
        <button
          title="Add an induction step after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "data-induction")
          }
        >
        Ind+
        </button>
        <button
          title="Add an equalities step after this step"
          onClick={
            () => this.props.addAfterHandler(this.props.number, false, "equalities")
          }
        >
        E+
        </button>
      </>
    );
  }

  getStyleClass(status) {
    status = status === undefined ?
      (Array.isArray(this.props.status) ?
        this.props.status[0] : this.props.status)
      : status;
    if (status === "No" || status === "Cases-not-covered") {
      return "result-error";
    }
    if (status === "Uh" || this.props.justification.type === "?") {
      return "result-tentative";
    }
    if (status === "Ok") {
      return "result-ok";
    }

    return "";
  }

  renderEqualitiesStep() {
    const renderEquality = (e, i) => (
      <tr key={i}>
        <td className="equality-equal-cell">=</td>
        <td className="equality-term-cell">
          <Formula
            formula={e.term}
            raw={e.raw}

            updateHandler={this.handleEqualityTermUpdate(i)}
          />
        </td>
        <td className="equality-justification-cell">
          <Justification
            justification={e.justification}

            updateHandler={this.handleEqualityJustificationUpdate(i)}
          />
        </td>
        <td className="equality-button-cell">
          <button
            title="Remove this term"
            onClick={this.handleRemoveEquality(i)}
          >
            X
          </button>
        </td>
      </tr>
    );

    return (
      <tr className={"step " + this.getStyleClass()}>
        <td className="step-status-cell">
          {this.props.status}
        </td>
        <td className="step-number-cell">
          {this.renderNumber()}
        </td>
        <td className="step-formula-cell">
          <Formula
            formula={this.props.term}
            raw={this.props.raw}

            updateHandler={this.handleFormulaUpdate}
          />
        </td>
        <td>
          <table>
            <tbody>
              {this.props.equalities.map(renderEquality)}
              <tr>
                <td>
                  <button
                    title="Add a new term"
                    onClick={this.handleAddEquality}
                  >
                    +
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        <td className="step-operation-cell">
        {this.renderDeleteButton()}
        {this.renderActionButtons()}
        </td>
      </tr>
    );
  }

  renderInductionStep() {
    const renderInductionPremise = (caseIndex) => (p, i) => (
      <div
        key={i}
        className="assumption-header"
      >
        <button
          title="Delete this induction hypothesis"
          onClick={this.handleRemovePremise(caseIndex, i)}
        >
          X
        </button>
        <span className="assumption-text">Take</span>
        <Declaration
          updateHandler={this.handlePremiseIntroductionUpdate(caseIndex, i)}
          type="term"
          name={p.term}
          termType={p.termType}
          raw={p.declarationRaw}/>
        { p.assumption !== undefined ? 
          (<span className="assumption-text">  such that</span>) : (null) }
        { p.assumption !== undefined ? 
          <Formula
            formula={p.assumption}
            raw={p.raw}
            updateHandler={this.handlePremiseFormulaUpdate(caseIndex, i)}
          /> : (null) }
      </div>
    );
    const renderInductionCase = (c, i) => (
      <div className="assumption" key={i}>
        <div className={
            this.props.status === undefined ?
              "" : this.getStyleClass(this.props.status[i + 1])
          }>
          <span className="induction-case-header">
            { c.premises.every((p) => p.assumption === undefined) ?
              "Base case" : "Inductive Step"
            }
          </span>
          { c.premises.map(renderInductionPremise(i)) }
          <br/>
          <button
            title="Introduce a new variable for the case"
            onClick={this.handleAddPremise(i, false)}
          >
            P+I
          </button>
          <button
            title="Introduce a new variable and an hypothesis for the case"
            onClick={this.handleAddPremise(i, true)}
          >
            P+IA
          </button>
          <br/>
          <span className="assumption-text">To show</span>
          <Formula
            formula={c.goal}
            raw={c.raw}
            updateHandler={this.handleCaseGoalUpdate(i)}
          />
          <div className="assumption-operations">
            <button
              title="Remove this case"
              onClick={this.handleRemoveCase(i)}
            >
              X
            </button>
            { c.trivial ? (
              <>
                <button
                  title="Turns this case into a non-trivial case with a subproof"
                  onClick={this.handleCaseFlip(i)}
                >
                  ->Subproof
                </button>
              </>
            ) : (
              <>
                <button
                  title="Turns this case into a trivial case that follows directly from some justifications"
                  onClick={this.handleCaseFlip(i)}
                >
                  ->Trivial
                </button>
                <button
                  title="Add a step to the beginnging of the subproof of this case"
                  onClick={this.handleCaseAddStep(i, "step")}
                >
                  +
                </button>
                <button
                  title="Add an assumption step to the beginnging of the subproof of this case"
                  onClick={this.handleCaseAddStep(i, "assumption")}
                >
                  A+
                </button>
                <button
                  title="Add an introduction step to the beginnging of the subproof of this case"
                  onClick={this.handleCaseAddStep(i, "introduction")}
                >
                  I+
                </button>
                <button
                  title="Add an introduction step with an assumption to the beginnging of the subproof of this case"
                  onClick={this.handleCaseAddStep(i, "introduction-with-assumption")}
                >
                  IA+
                </button>
                <button
                  title="Add an induction step after this step"
                  onClick={this.handleCaseAddStep(i, "data-induction")}
                >
                Ind+
                </button>
                <button
                  title="Add an equalities step after this step"
                  onClick={this.handleCaseAddStep(i, "equalities")}
                >
                E+
                </button>
              </>
            )
          }
          </div>
          { c.trivial ? (
            <>
              <br/>
              <span className="assumption-text">By</span>
              <Justification
                justification={c.justification}
                updateHandler={this.handleCaseJustificationUpdate(i)}
              />
            </>
          ) : (
            <table className="steps">
              <tbody>
                {this.renderSubsteps(c.subproof)}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
    const formula = this.props.formula;
    const on = this.props.type === "data-induction" ?
      (formula.type === "quantification"
        && this.props.inductiveTypes.includes(formula.termType)
        && formula.quantifier === "Forall" ? formula.termType
                                           : undefined) :
      this.props.definition;
    return (
      <tr className="assumption-box">
        <td className="assumption-body-cell" colSpan="5">
          <div className={"assumption " + this.getStyleClass()}>
            <div className="assumption-header">
              <span className="assumption-number">{this.renderNumber()}</span>
              <span className="assumption-text">Prove</span>
              <Formula
                formula={this.props.formula}
                raw={this.props.raw}

                updateHandler={this.handleFormulaUpdate}
              />
              <div className="assumption-operations">
                {this.props.type === "data-induction" ?
                  (<button
                    title="Make this step an induction over function definition"
                    onClick={() => this.handleFlipType()}>
                   ->Function
                   </button>) : (<button
                    title="Make this step an induction over data structure"
                    onClick={() => this.handleFlipType()}>
                   ->Data
                   </button>)}
                {this.renderDeleteButton()}
              </div>
              <br/>
              <span className="assumption-text">
                by induction on the definition
                {this.props.type === "data-induction" ? " of data structure" : ""}
              </span>
              {on === undefined ? (<span className="error-text">
                Error: induction goal is not a universal quantification over a data structure
              </span>) : 
                (this.props.type === "data-induction" ? (<MathJax.Node
                  className="formula"
                  formula={"\\color{blue}{\\texttt{" + on + "}}"}/>)
                  : 
                  (this.state.editDefinition ?
                    (<input
                      ref={this.definitionInput}
                      onChange={this.handleDefinitionChange}
                      onKeyPress={this.handleDefinitionKeyPress}
                      onBlur={() => this.finishEditDefinition()}
                      type="text"
                      value={this.state.definition}
                    />) : (<span
                      className="assumption-number"
                      onClick={() => this.startEditDefinition()}
                    >
                      {on}
                    </span>)))
              }
            </div>
            <br/>
            {(this.props.status && this.props.status[0] === "Cases-not-covered" ?
              (<span className="error-text">
                Error: Induction principle is incorrect
              </span>) : this.props.status
                          && this.props.status[0] !== undefined ?  (<span>
                Induction principle is correct
              </span>) : (null))}
            { this.props.cases.map(renderInductionCase) }
            <button
              title="Add a new case"
              onClick={this.handleAddCase}
            >
              Add Case
            </button>
            <br/>
            {this.renderActionButtons()}
          </div>
        </td>
      </tr>
    );
  }

  renderNormalStep() {
    return (
      <tr className={"step " + this.getStyleClass()}>
        <td className="step-status-cell">
          {this.props.status}
        </td>
        <td className="step-number-cell">
          {this.renderNumber()}
        </td>
        <td className="step-formula-cell">
          <Formula
            formula={this.props.formula}
            raw={this.props.raw}

            updateHandler={this.handleFormulaUpdate}
          />
        </td>
        <td className="step-justification-cell">
          {this.renderJustification()}
        </td>
        <td className="step-operation-cell">
        {this.renderInductionButton()}
        {this.renderDeleteButton()}
        {this.renderActionButtons()}
        </td>
      </tr>
    );
  }

  renderSubsteps(subproof) {
    if (subproof === undefined) {
      subproof = this.props.subproof;
    }
    if (subproof.length === 0) {
      return (
        <span>
          No subproof
        </span>
      );
    }
    return subproof.map((step) => {
      return(
      <Step
        key={step.key}

        inductiveTypes={this.props.inductiveTypes}

        addMessageHandler={this.props.addMessageHandler}

        deleteHandler={this.props.deleteHandler}
        addAfterHandler={this.props.addAfterHandler}

        updateHandler={this.props.updateHandler}

        type={step.type}
        status={step.status === undefined ? "" : step.status}
        number={step.number}

        formula={step.formula}
        assumption={step.assumption}
        subproof={step.subproof}
        raw={step.raw}

        term={step.term}
        termType={step.termType}
        declarationRaw={step.declarationRaw}
        declarationType={step.declarationType}
        cases={step.cases}
        definition={step.definition}
        equalities={step.equalities}

        justification={step.justification}
      />) }
    );
  }

  renderIntroductionWithAssumption() {
    return (
      <tr className="assumption-box">
        <td className="assumption-body-cell" colSpan="5">
          <div className="assumption">
            <div className="assumption-header">
              <span className="assumption-number">{this.renderNumber()}</span>
              <span className="assumption-text">Take</span>
              <Declaration
                updateHandler={this.handleIntroductionUpdate}
                type={this.props.declarationType}
                name={this.props.term}
                termType={this.props.termType}
                raw={this.props.declarationRaw}/>
              <span className="assumption-text">  such that</span>
              <Formula
                formula={this.props.assumption}
                raw={this.props.raw}

                updateHandler={this.handleFormulaUpdate}
              />
              <div className="assumption-operations">
                {this.renderDeleteButton()}
                {this.renderActionButtons(true)}
              </div>
            </div>
            <table className="steps">
              <tbody>
                {this.renderSubsteps()}
              </tbody>
            </table>
            <br/>
            {this.renderActionButtons()}
          </div>
        </td>
      </tr>
    );
  }

  renderIntroduction() {
    return (
      <tr className="assumption-box">
        <td className="assumption-body-cell" colSpan="5">
          <div className="assumption">
            <div className="assumption-header">
              <span className="assumption-number">{this.renderNumber()}</span>
              <span className="assumption-text">Take</span>
              <Declaration
                updateHandler={this.handleIntroductionUpdate}
                type={this.props.declarationType}
                name={this.props.term}
                termType={this.props.termType}
                raw={this.props.declarationRaw}/>
              <div className="assumption-operations">
                {this.renderDeleteButton()}
                {this.renderActionButtons(true)}
              </div>
            </div>
            <table className="steps">
              <tbody>
                {this.renderSubsteps()}
              </tbody>
            </table>
            <br/>
            {this.renderActionButtons()}
          </div>
        </td>
      </tr>
    );
  }

  renderAssumption() {
    return (
      <tr className="assumption-box">
        <td className="assumption-body-cell" colSpan="5">
          <div className="assumption">
            <div className="assumption-header">
              <span className="assumption-number">{this.renderNumber()}</span>
              <span className="assumption-text">Assume</span>
              <Formula
                formula={this.props.assumption}
                raw={this.props.raw}

                updateHandler={this.handleFormulaUpdate}
              />
              <div className="assumption-operations">
                {this.renderDeleteButton()}
                {this.renderActionButtons(true)}
              </div>
            </div>
            <table className="steps">
              <tbody>
                {this.renderSubsteps()}
              </tbody>
            </table>
            <br/>
            {this.renderActionButtons()}
          </div>
        </td>
      </tr>
    );
  }

  render() {
    switch (this.props.type) {
      case "equalities": return this.renderEqualitiesStep();
      case "function-induction":
      case "data-induction": return this.renderInductionStep();
      case "step": return this.renderNormalStep();
      case "introduction-with-assumption":
        return this.renderIntroductionWithAssumption();
      case "introduction": return this.renderIntroduction();
      case "assumption": return this.renderAssumption();
      default: throw new Error("Bad step");
    }
  }
}

export default Step;
